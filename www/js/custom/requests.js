var req = new XMLHttpRequest();
req.onreadystatechange = function(){
    if (req.readyState==4 && req.status==200){
        location.reload();
        alert("Дякуємо за заявку! Вона буде оброблена найближчим часом!");
    }
};

function sendMessage(){
    var name = $("#name").val();
    var street = $("#street option:selected").text();
    var building = $("#building option:selected").text();
    var phone = $("#phone").val();
    var email = $('#email').val();
    var message = $("#message").val();

    if (!checkName() || !checkStreet() || !checkBuilding() || !checkPhone() || !checkMail()){
        return;
    }

    var content = new Object();
    content.name = name;
    content.phone = phone;
    content.email = email;
    content.street = street;
    content.building = building;
    content.message = message;
    content.date = new Date();

    var json=JSON.stringify(content);
    req.open("POST", "/php/saveRequestToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(json);
}

function checkName(){
    var name = $("#name").val();
    var incorrName  = $('#incorrectName');
    if (name===""){
        incorrName.html("Поле \"ім\'я\" не може бути пустим.");
        return false;
    }
    else{
        var regExpr = /^([A-ZА-ЯІa-zа-яі-]+[ ]*){1,3}$/;
        if (!regExpr.test(name)){
            incorrName.html("Поле \"ім\'я\" може містити тільки літери та пробіли і складатись не більше, чим з трьох слів.");
            return false;
        }
        else{
            incorrName.html("");
            return true;
        }
    }
}

function checkStreet(){
    var street = $("#street option:selected").text();

    if (street===""){
        $('#incorrectStreet').html("Виберіть вулицю.");
        return false;
    }
    else{
        $('#incorrectStreet').html("");
        return true;
    }
}

function checkBuilding(){
    var building = $("#building option:selected").text();

    if (building===""){
        $('#incorrectBuilding').html("Виберіть будинок.");
        return false;
    }
    else{
        $('#incorrectBuilding').html("");
        return true;
    }
}

function checkPhone(){
    var name = $("#phone").val();
    var incorrName  = $('#incorrectPhone');
    if (name===""){
        incorrName.html("Поле \"Номер телефону\" не може бути пустим.");
        return false;
    }
    else {
        var regExpr = /^[ ]*((38|8|\+38)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
        if (!regExpr.test(name)){
            incorrName.html("Поле \"Номер телефону\" має бути у вигляді \"+380001234567\".");
            return false;
        }
        else{
            incorrName.html("");
            return true;
        }
    }
}

function checkMail(){
    var name = $("#email").val();
    var incorrName  = $('#incorrectMail');
    var regExpr = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (name!= "" && !regExpr.test(name)){
        incorrName.html("Поле \"E-mail\" має бути у вигляді \"example@ukr.net\"");
        return false;
    }
    else {
        incorrName.html("");
        return true;
    }
}