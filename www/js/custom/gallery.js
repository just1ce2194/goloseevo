function loadAllPhotos(){
    var req = new XMLHttpRequest();
    req.open("POST", "/php/getAllPhotos.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(null);

    var res = JSON.parse(req.responseText);
    var gallery = $('#gallery');
    var map = new Map();
    var j=0;
    for (var i=0;i<res.length;i++){
        //alert(res[i]['link']+" "+res[i]['description']+" "+res[i]['album']);
        if (!map.has(res[i]['album'])){
            map.set(res[i]['album'],j);
            gallery.append($('<div class="album" data-jgallery-album-title="'+res[i]['album']+'" id="alb'+j+'"></div>'));
            j++;
        }
        else{
        }
        $('#alb'+map.get(res[i]['album'])).append($('<a hidden href="'+res[i]['link'].substring(res[i]['link'].indexOf("images"))+'"><img src="'+res[i]['link'].substring(res[i]['link'].indexOf("images"))+'" alt="'+res[i]["description"]+'"/></a>'));
    }
}