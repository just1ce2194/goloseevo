function getStreets(){
    var request = new XMLHttpRequest();
    request.open("POST", "/php/getStreets.php", false);
    request.send(null);
    if (request.responseText.length===0){
        return;
    }
    var res = JSON.parse(request.responseText);
    for (var i=0;i<res.length;i++){
        $('#street').append($('<option value="'+res[i]['NAME']+'">'+res[i]['NAME']+'</option>'))
    }
}

function loadBuildings(){
    $('#delete').remove();
    var changedStreet = $("#street option:selected").text();

    var req = new XMLHttpRequest();
    var content = new Object();
    content.street = changedStreet;

    var json = JSON.stringify(content);
    req.open("POST", "/php/getBuildingsByStreet.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(json);

    var res = JSON.parse(req.responseText);
    $('#building').empty();
    for (var i=0;i<res.length;i++){
        $('#building').append('<option value="' + res[i] + '">' + res[i] + '</option>');
    }
}