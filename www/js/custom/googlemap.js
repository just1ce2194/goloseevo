var map;
var infoWindow;

function initMap(k) {
    map = new google.maps.Map(document.getElementById('map'), {
        maxZoom:19,
        minZoom:k,
        zoom:14,
        center: {lat: 50.391683, lng: 30.496768},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var triangle = {
        A:[50.380366, 30.477874],
        B:[50.394961, 30.513017],
        C:[50.392928, 30.485134]
    };

    google.maps.event.addListener(map, 'dragend', function() {
        if (!pointInTriangle(map.getCenter().lat(),map.getCenter().lng(),triangle.A[0],triangle.A[1],triangle.B[0],triangle.B[1],triangle.C[0],triangle.C[1])){
            map.setCenter(new google.maps.LatLng(50.391683, 30.496768));
        }
    });

    //выделение голосеево контуром----------------
    var goloseevoCoordinates = [
        {lat: 50.397583, lng: 30.508874},
        {lat: 50.396539, lng: 30.505080},
        {lat: 50.395572, lng: 30.501409},
        {lat: 50.392999, lng: 30.486357},
        {lat: 50.392612, lng: 30.484901},
        {lat: 50.391993, lng: 30.484172},
        {lat: 50.383073, lng: 30.478255},
        {lat: 50.381738, lng: 30.479287},
        {lat: 50.391993, lng: 30.504565},
        {lat: 50.393057, lng: 30.506628},
        {lat: 50.395165, lng: 30.508540},
        {lat: 50.397216, lng: 30.509814},
        {lat: 50.397583, lng: 30.508752}
    ];
    var goloseevo = new google.maps.Polyline({
        path: goloseevoCoordinates,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 0.6,
        strokeWeight: 4
    });

    goloseevo.setMap(map);
    //-------------------------------------------
    //-----markers
    request = new XMLHttpRequest();
    request.open("POST", "/php/getMarkers.php", false);
    request.send(null);
    my_JSON_object = JSON.parse(request.responseText);
    my_JSON_object.forEach(function(marker){
        var link = marker['link'];
        var image = {
            url: link.substring(link.indexOf('images'),link.length),
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(24, 24),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 24)
        };
        var marker1 = new google.maps.Marker({
            position: {lat: parseFloat(marker['lat']), lng: parseFloat(marker['lng'])},
            map: map,
            icon: image
        });
        marker1.setMap(map);
        marker1.addListener('click', function () {
            var contentString = marker['description'];
            infoWindow.setContent(contentString);
            infoWindow.setPosition({lat: parseFloat(marker['lat']), lng: parseFloat(marker['lng'])});
            infoWindow.open(map);
        });
    });
    //--------




    var request = new XMLHttpRequest();
    request.open("POST", "/php/getPolygons.php", false);
    request.send(null);
    var my_JSON_object = JSON.parse(request.responseText);
    my_JSON_object.forEach(function(building){
        var street = building['street'];
        var number = building['number'];
        var center = building['center'];

        var temp = center.split(", ");
        if (temp.length != 2) {
            return;
        }
        center = [parseFloat(temp[0].replace(',', '.')), parseFloat(temp[1].replace(',', '.'))];

        var coordinates = building['coordinates'];
        temp = coordinates.split("; ");
        coordinates = [];
        for (var i=0;i<temp.length-1;i++){
            var coord = temp[i].split(", ");
            if (coord.length != 2) {
                return;
            }
            coordinates.push({lat: parseFloat(coord[0].replace(',', '.')), lng: parseFloat(coord[1].replace(',', '.'))});
        }

        var picture = building['picture'];
        var info = building['info'];
        var linktoforum = building['linktoforum'];

        var building1 = new google.maps.Polygon({
            paths: coordinates,
            strokeColor: '#a07dff',
            strokeOpacity: 0.4,
            strokeWeight: 3,
            fillColor: '#c7b3ff',
            fillOpacity: 0.2
        });
        building1.setMap(map);
        building1.addListener('click', function () {
            var contentString = '<FONT face="Helvetica Neue" size=3pt ><b>'+street+
                ', будинок '+number+'</b><br><br><img src="'+picture+
                '" alt="Smiley face" height="100" width="100" align="left" hspace="8">'+
                info+'<br><a href="'+linktoforum+'">Перейти на форум</a></FONT>';
            infoWindow.setContent(contentString);
            infoWindow.setPosition({lat: center[0], lng: center[1]});
            infoWindow.open(map);
        });

    });
    infoWindow = new google.maps.InfoWindow;
}

function sign (ax,ay,bx,by,cx,cy)
{
    return (ax - cx) * (by - cy) - (bx - cx) * (ay - cy);
}

/**
 * @return {boolean}
 */
function pointInTriangle (px,py,ax,ay,bx,by,cx,cy)
{
    var b1 = sign(px, py, ax, ay, bx, by) < 0.0;
    var b2 = sign(px, py, bx, by, cx, cy) < 0.0;
    var b3 = sign(px, py, cx, cy, ax, ay) < 0.0;

    return ((b1 === b2) && (b2 === b3));
}