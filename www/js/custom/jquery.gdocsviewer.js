/*
 * jQuery.gdocViewer - Embed linked documents using Google Docs Viewer
 * Licensed under MIT license.
 * Date: 2011/01/16
 *
 * @author Jawish Hameed
 * @version 1.0
 */
(function($){
	$.fn.gdocsViewer = function(options) {
	
		var settings = {
			width  : '600',
			height : '700'
		};
		
		if (options) { 
			$.extend(settings, options);
		}
		
		return this.each(function() {
			var file = $(this).attr('href');
			var ext = file.substring(file.lastIndexOf('.') + 1);

			if (/^(tiff|pdf|ppt|pps|doc|docx)$/.test(ext)) {
				$(this).after(function () {
					var id = $(this).attr('id');
					var gdvId = (typeof id !== 'undefined' && id !== false) ? id + '-gdocsviewer' : '';
					return '<div id="' + gdvId + '" class="gdocsviewer" style="display: none"><iframe src="http://docs.google.com/viewer?embedded=true&url=' + encodeURIComponent(file) + '" width="' + settings.width + '" height="' + settings.height + '" style="border: none;"></iframe></div>';
				})
			}
		});
	};
})( jQuery );


function PopUpShow(id){
	var html = '<a href="javascript:PopUpHide()">';
	html += '<div>';
	html += document.getElementById(id + "-gdocsviewer").innerHTML;
	html += '<div>';
	html += '</a>';
	$(html).appendTo("#popupDoc");
	$("#popupDoc").show();
}
function PopUpHide(){
	$("#popupDoc").hide();
	document.getElementById("popupDoc").innerText = "";
}

$(document).ready(function($) {
	PopUpHide();
	$('#popupDoc').css('margin-top',$('.navbar').height());
	$('label.embed').gdocsViewer({width: "100%", height: "95%"});
});
