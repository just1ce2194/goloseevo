function loadNews(type){
    var content = new Object();
    content.type = type;
    var request = new XMLHttpRequest();
    request.open("POST", "/php/getNews.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText.length===0){
        return;
    }

    var res = JSON.parse(request.responseText);
    if (res.length!=0){
        var div = $('#'+type+'_news');
        var ul = $('<ul style="z-index: 2" class="demo1"></ul>');
        div.empty();
        var li;
        for (var i = 0; i < res.length; i++) {
            li = $('<li class="news-item"><table cellpadding="4"><tr>' +
                '<td><img src="http://kalinichenko.com.ua/' + res[i]['image'].substring(res[i]['image'].indexOf('images')) + '" width="100px" class="img-related" hspace=4 /></td>' +
                '<td>' + res[i]['title'] + '<br><a id="' + type + res[i]['id'] + '" style="cursor:pointer;" onclick="readMore(this)">Читати більше...</a></td>' +
                '</tr></table></li>');
            div.append(li);
        }
    }
}

function readMore(e){
    var id = parseInt(e.id.replace(/\D+/g,""));
    var type = e.id.substring(0, e.id.indexOf(id));
    var content = new Object();
    content.type = type;
    content.id = id;
    var request = new XMLHttpRequest();
    request.open("POST", "/php/getOneNewsById.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));
    var res = JSON.parse(request.responseText);
    var height = $('#mapbody').height();
    var html = '<div class="row" style="height:'+height+'px;">';
    html +='<div class="col-md-12" style="height:'+height+'px; min-height: 380px;">';
    html +='<div class="panel panel-default">';
    html +='<div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>'+res[0]['title']+'</b></div>';
    html +='<div class="panel-body" >';
    html +='<div class="row">';

    html +='<div class="col-xs-12 col-md-12" id="pb">';
    html += '<div class="row">';
    html += '<div class="col-xs-4 col-md-4">';
    html += '<div class="row"><br><br><img class="img-responsive img-thumbnail" src="http://kalinichenko.com.ua/' + res[0]['image'].substring(res[0]['image'].indexOf("images")) + '"></div>';
    html += '<div class="row">'+res[0]['date']+'</div>';
    html += '<div class="row"><button type="button" class="btn btn-primary" onclick="hideNews()">Закрити</button></div></div>';
    html += '<div class="col-xs-8 col-md-8">';
    html += '<div class="row"><br><div id="text_news" style="line-height:2em;overflow:auto;padding:5px;">'+res[0]['text']+'</div></div>';

    html +='</div></div></div></div></div><div class="panel-footer"></div></div></div>';

    //$('#text_news').css('height',$('#mapbody').height()-20);



    document.getElementById("news_block").innerText = "";
    $(html).appendTo("#news_block");
    $('#text_news').css('height',$('#pb').height()*0.82);


    /*var html = '<table align="center" class="contenttable" cellpadding="0" cellspacing="0" border="0" style="margin-top: 10%;background-color: white; min-width: 500px; min-height: 200px;z-index: 20">';
    html += '<tr><td colspan="2"> <h1 align="center">'+res[0]['title']+'</h1> </td></tr>';
    html += '<tr><td><img style="max-width: 150px" src="http://goloseevo/'+res[0]['image'].substring(res[0]['image'].indexOf("images"))+'"></td> <td>'+res[0]['text']+'</td></tr>';
    html += '<tr><td style="font-style: italic">'+res[0]['date']+'</td></tr>';
    html += '<tr><td><a href=javascript:hideNews()>Закрити</a></td></tr>';
    html += '</table>';

    $("#popupNews").show();*/
return false;
    //alert("Title: "+res[0]['title']+",\n image: "+res[0]['image']+",\n text: "+res[0]['text']+",\n date: "+res[0]['date']);
}

function hideNews() {
    var html = '<div class="row"><div class="col-md-12 actualnewsbody" style="height: 100%;"><div class="panel panel-default"><div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Актуальні новини</b></div>';
    html+='<div class="panel-body actual-news-panel-body"><div class="row"><div class="col-xs-12" id="pb"><ul class="demo1" id="actual_news" style="z-index: 2;position: relative;"  ></ul></div></div></div><div class="panel-footer" style="z-index: 10;position: relative;"> </div>';
    html+='</div></div></div><div class="row"><div class="col-md-6 newsbody" style="height: 100%;"><div class="panel panel-default"><div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Виконані роботи</b></div>';
    html+='<div class="panel-body pd-news-panel-body"><div class="row"><div class="col-xs-12"><ul class="demo1" id="done_news" ></ul></div></div></div><div class="panel-footer"> </div>';
    html+='</div></div><div class="col-md-6 newsbody" style="height: 100%;"><div class="panel panel-default"><div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Заплановані роботи</b></div>';
    html+='<div class="panel-body pd-news-panel-body" id="newspanel"><div class="row"><div class="col-xs-12"><ul class="demo1" id="planned_news" ></ul></div></div></div><div class="panel-footer"></div></div></div></div>';
    document.getElementById("news_block").innerText = "";
    $(html).appendTo("#news_block");
    $('.newsbody').css('height',$('#mapbody').height()*0.5);
    $('#actual_news').css('height',$('#news_space').height());
    $('.actualnewsbody').css('height',$('#mapbody').height()*0.5);
    $('.actual-news-panel-body').css('height',$('#mapbody').height()*0.5 - 120);
    $('.pd-news-panel-body').css('height',$('#mapbody').height()*0.5 - 120);
    loadNews('done'); loadNews('planned');
    loadNews('actual');

    $(".demo1").bootstrapNews({
        newsPerPage: 2,
        autoplay: false,
        pauseOnHover:true,
        direction: 'up',
        newsTickerInterval: 4000,
        onToDo: function () {
            //console.log(this);
        }
    });

    $(".demo2").bootstrapNews({
        newsPerPage: 4,
        autoplay: false,
        pauseOnHover: true,
        //navigation: false,
        //direction: 'down',
        //newsTickerInterval: 2500,
        onToDo: function () {
            //console.log(this);
        }
    });

    $("#demo3").bootstrapNews({
        newsPerPage: 3,
        autoplay: false,

        onToDo: function () {
        }
    });

}

