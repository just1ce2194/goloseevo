$(document).ready ( function(){
    $("#popupNews").hide();
    var cw = $('div.changablepic').width();
    $('div.changablepic').css({
        'height': cw + 'px'
    });
//        $("#map").css({height : $("#newspanel").height() - 18 + 'px'});
//Set default resolution and font size
    var resolution = 1024;
    var font = 30;

    //Get window width
    var width = $(window).width();

    //Set new font size
    var newFont = font * (width/resolution);
    $('div.headmaintext').css('font-size', newFont);
    $('div.headmaintext').css({
        'font-size': newFont + 'px'
    });

    font = 15;
    newFont = font * (width/resolution);
    $('div.headtextunderphoto').css('font-size', newFont);
    $('div.headtextunderphoto').css({
        'font-size': newFont + 'px'
    });

    $('.newsbody').css('height',$('#mapbody').height()*0.5);
    $('#actual_news').css('height',$('#news_space').height());
    $('.actualnewsbody').css('height',$('#mapbody').height()*0.5);
    $('.actual-news-panel-body').css('height',$('#mapbody').height()*0.5 - 120);
    $('.pd-news-panel-body').css('height',$('#mapbody').height()*0.5 - 120);
    $('.kyiv-news').css('height',$('.problem-form').height() - $('.panel-heading').height() - 40);

    if ($(window).width() > 960) {
        $('.index-gallery-img').css('height', $(window).width() / 6 - 5);
    }
    else {
        $('.index-gallery-img').css('height', '100%');
    }
});

$(window).resize ( function(){
    var cw = $('div.changablepic').width();
//        alert(cw);
    $('div.changablepic').css({
        'height': cw + 'px'
    });
//        $("#map").css({height : $("#newspanel").height() - 18 + 'px'});
    //Set default resolution and font size
    var resolution = 1024;
    var font = 30;

    //Get window width
    var width = $(window).width();

    //Set new font size
    var newFont = font * (width/resolution);
    $('div.headmaintext').css('font-size', newFont);
    $('div.headmaintext').css({
        'font-size': newFont + 'px'
    });

    font = 15;
    newFont = font * (width/resolution);
    $('div.headtextunderphoto').css('font-size', newFont);
    $('div.headtextunderphoto').css({
        'font-size': newFont + 'px'
    });

    $('.newsbody').css('height',$('#mapbody').height()*0.5);
    $('.actualnewsbody').css('height',$('#mapbody').height()*0.5);
    $('.actual-news-panel-body').css('height',$('#mapbody').height()*0.5 - 120);
    $('.pd-news-panel-body').css('height',$('#mapbody').height()*0.5 - 120);

    if ($(window).width() > 960) {
        $('.index-gallery-img').css('height', $(window).width() / 6 - 5);
    }
    else {
        $('.index-gallery-img').css('height', '100%');
    }
});

$(function () {
    loadNews('done'); loadNews('planned');
    loadNews('actual');

    $(".demo1").bootstrapNews({
        newsPerPage: 2,
        autoplay: false,
        pauseOnHover:true,
        direction: 'up',
        newsTickerInterval: 4000,
        onToDo: function () {
            //console.log(this);
        }
    });

    $(".demo2").bootstrapNews({
        newsPerPage: 4,
        autoplay: false,
        pauseOnHover: true,
        //navigation: false,
        //direction: 'down',
        //newsTickerInterval: 2500,
        onToDo: function () {
            //console.log(this);
        }
    });

    $("#demo3").bootstrapNews({
        newsPerPage: 3,
        autoplay: false,

        onToDo: function () {
            //console.log(this);
        }
    });
});

$('.carousel').carousel({
    interval: 5000 //changes the speed
});