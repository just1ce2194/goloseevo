<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.3.4
 * Remote API Server configuration
 * Last Updated: $Date: 2012-06-12 10:14:49 -0400 (Tue, 12 Jun 2012) $
 * </pre>
 *
 * @author 		$Author: bfarber $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		Nulled by IPBZona.ru
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @version		$Rev: 10914 $
 *
 */

$CONFIG['api_module_title'] = 'IP.Board';
$CONFIG['api_module_desc']  = 'API methods for interfacing with IP.Board';
$CONFIG['api_module_key']   = 'ipb';
