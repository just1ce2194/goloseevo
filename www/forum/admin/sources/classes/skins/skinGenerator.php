<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.3.4
 * Skin Functions
 * Last Updated: $Date: 2011-05-05 12:03:47 +0100 (Thu, 05 May 2011) $
 * </pre>
 *
 * Owner: Matt
 * @author 		$Author: ips_terabyte $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		Nulled by IPBZona.ru
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		9th March 2005 11:03
 * @version		$Revision: 8644 $
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class skinGenerator extends skinCaching
{
	/**#@+
	 * Registry objects
	 *
	 * @access	protected
	 * @var		object
	 */	
	protected $registry;
	protected $DB;
	protected $settings;
	protected $request;
	protected $lang;
	protected $member;
	protected $memberData;
	protected $cache;
	protected $caches;
	/**#@-*/
	
	
	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	object		Registry object
	 * @return	@e void
	 */
	public function __construct( ipsRegistry $registry )
	{
		/* Make object */
		parent::__construct( $registry );
	}
	
	/**
	 * Return JSON payload URL
	 * @return string Skin Generator URL
	 */
	public function getJsonUrl()
	{
		$inDev = ( IN_DEV ) ? 1 : 0;
		
		return 'http://ips-skin-gen.invisionpower.com/index.php?v=' . IPB_LONG_VERSION . '&k=' . urlencode( $this->settings['ipb_reg_number'] ) . '&i=' . $inDev;
	}
	
	/**
	 * Delete a user's session
	 * @param int $skinSetId
	 */
	public function convertToFull( $skinSetId )
	{
		/* Current session? */
		$session = $this->getSessionBySkinId( $skinSetId );
		
		if ( $session['sg_member_id'] )
		{
			$this->deleteUserSession( $session['sg_member_id'] );
		}
		
		/* Tweak skin */
		$this->DB->update( 'skin_collections', array( 'set_by_skin_gen' => 0 ), 'set_id=' . $skinSetId );
	}
	
	
	/**
	 * Check to make sure we're GTG ...
	 */
	public function healthCheck()
	{
		$warnings = array();
		
		/* Can write into images */
		if ( ! is_writable( DOC_IPS_ROOT_PATH . PUBLIC_DIRECTORY . '/style_images' ) )
		{
			$warnings[] = 'Cannot write to image directory';
		}
		
		return ( count( $warnings ) ) ? $warnings : true;
	}
	
	/**
	 * Fetches the JSON from the remote server and writes it
	 */
	public function buildLocalJsonCache()
	{
		/* Now fetch JSON data from the server and write it */
		require_once( IPS_KERNEL_PATH . 'classFileManagement.php' );/*noLibHook*/
		$files = new classFileManagement();
		
		$json = <<<SKINGENRUS
var IPS_SKIN_GEN_DATA = {"skinGroups":{"app_calendar":["Приложение: Календарь",["app_calendar_td_selected","app_calendar_td_hover","app_calendar_td_blank","app_calendar_td_today"]],"global":["Глобальные стили",["body","a","content","desc","desclighter"]],"tables":["Таблицы",["row1","row2","unread","unreadalt","tableheader","tableheader_a","ipb_table"]],"other":["Прочие",["ipsbox","Bar Header","postblock","NoMessages","fieldsetSubmit","general_box","general_box_h3","ipsboxcontainer","maintitle","sideMenuActive","ipsVerticalTabbed_tabs_li","ipsVerticalTabbed_tabs_li_a","ipsVerticalTabbed_tabs_li_a_hover","ipsVerticalTabbed_tabs_li_active","ipsfilterbar_item","ipsfilterbar_active"]],"buttons":["Кнопки",["but_submit","but_submit_alt","but_topic","but_secondary"]],"header":["Шапка",["registerLink","header","primarynav","primarynav_inactive","primarynav_active","searchbutton"]],"pagination":["Пагинация",["paginationBackForward","paginationBackForwardHover","paginationBackActive"]],"sidebar":["Боковой блок",["ipsSideBlock","ipsSideBlock_h3"]],"profiles":["Профиль",["statusupdate"]],"app_blog":["Приложение: Блог",["entry_header"]],"app_gallery":["Приложение: Галерея",["albumDetailTable_td"]]},"colorizeGroups":{"base":["app_calendar_td_selected","app_calendar_td_hover","app_calendar_td_blank","app_calendar_td_today","body","content","row1","row2","unread","unreadalt","ipsbox","Bar Header","tableheader","tableheader_a","postblock","NoMessages","but_submit_alt","fieldsetSubmit","registerLink","header","primarynav","primarynav_inactive","primarynav_active","general_box","general_box_h3","ipsboxcontainer","maintitle","desc","desclighter","ipb_table","ipsVerticalTabbed_tabs_li","ipsVerticalTabbed_tabs_li_a","ipsVerticalTabbed_tabs_li_a_hover","ipsVerticalTabbed_tabs_li_active","ipsfilterbar_item","ipsfilterbar_active","but_secondary","paginationBackForward","ipsSideBlock","ipsSideBlock_h3","statusupdate","entry_header","albumDetailTable_td"],"text":["a"],"secondary":["but_submit","searchbutton","but_topic"],"tertiary":["sideMenuActive","paginationBackForwardHover","paginationBackActive"]},"classes":{"app_calendar_td_selected":["#calendar_table td.selected","Выбранная клетка календаря","Используется для оформления выбранной ячейки календаря на месяц",{"background":["#E2E9F0",""],"shadow":["#E2E9F0",""]}],"app_calendar_td_hover":["#calendar_table td.normal:hover","Клетка календаря при наведении курсора мыши ","Используется для оформления ячеек месячного календаря при наведении курсора мыши",{"background":["#EDF1F5",""]}],"app_calendar_td_blank":["#calendar_table td.blank","Пустая клетка календаря","Используется для оформления пустых ячеек месячного календаря",{"background":["#dbe2e8",""],"border":["#dbe2e8",""]}],"app_calendar_td_today":["#calendar_table td.today","Клетка сегодняшней даты календаря","Используется для оформления ячейки сегодняшней даты в месячном календаре",{"background":["#f1f6ec",""],"border":["#6f8f52",""],"color":["#6f8f52",""]}],"body":["html, body","Тело страницы","Оформление подложки страниц",{"background":["#d8dde8",""],"color":["#5a5a5a",""]}],"a":["a","Ссылки","Оформление ссылок",{"color":["#225985",""]}],"content":["#content","Основная область с данными","Оформление области в которой находятся все данные",{"background":["#fff",""]}],"row1":[".row1, .post_block.row1","Ряд таблицы 1","Оформления ряда таблицы цвета 1",{"background":["#fff",""]}],"row2":[".row2, .post_block.row2","Ряд таблицы 2","Оформления ряда таблицы цвета 2",{"background":["#f1f6f9",""]}],"unread":[".unread","Ряд с непрочтенным","Оформление ряда с непрочтенным",{"background":["#f7fbfc",""]}],"unreadalt":[".unread .altrow, .unread.altrow","Ряд с непрочтенным (альтернативный)","Более темное оформление ряда с непрочтенным",{"background":["#E2E9F0",""]}],"ipsbox":[".ipsBox","Блок данных","Используется для оборачивания секций с данными",{"background":["#ebf0f3",""]}],"Bar Header":[".bar","Полоска заголовка","Используется для оформления плашек в формах отправки и т.п.",{"background":["#eff4f7",""]}],"tableheader":[".header","Заголовки таблиц","Оформление заголовков таблиц",{"background":["#b6c7db",""],"color":["#1d3652",""]}],"tableheader_a":["body .ipb_table .header a,\tbody .topic_options a","Ссылки в заголовках таблиц","Оформление ссылок в заголовках таблиц",{"color":["#1d3652",""]}],"postblock":[".post_block","Блок сообщения","Оформление индивидуального блока сообщения",{"background":["#fff",""],"border":["#D6E2EB",""]}],"NoMessages":[".no_messages","Блок отсутсвия данных","Оформления блока сообщений об отсутствии данных",{"background":["#f6f8fa",""],"border":false}],"but_submit":[".input_submit","Кнопка отправки","Оформление основной кнопки отправки форм",{"background":["#212121",""],"border":["#5c5c5c",""],"color":["#fff",""],"boxshadow":["#5c5c5c"," inset 0 1px 0 0 "]}],"but_submit_alt":[".input_submit.alt","Альтернативная кнопка отправки","Оформление альтернативной кнопки отправки форм",{"background":["#e2e9f0",""],"border":["#dae2ea",""],"color":["#464646",""],"boxshadow":["#eff3f8","inset 0 1px 0 0 "]}],"fieldsetSubmit":["body#ipboard_body fieldset.submit,body#ipboard_body p.submit","Подложка кнопок отправки","Оформление подложки под кнопками отправки в формах",{"background":["#d1ddea",""]}],"registerLink":["#user_navigation #register_link","Ссылка регистрации","Оформление кнопки регистрации",{"background":["#7ba60d",""],"border":["#7ba60d",""],"color":["#fff",""],"boxshadow":false}],"header":["#branding","Плашка заголовка","Оформление полосы содержащей лого, поле поиска и т.п.",{"background":["#0f3854",""],"border":["#1b3759",""]}],"primarynav":["#primary_nav","Основная навигация","Оформление полоски навигации (в ней расположены закладки приложений)",{"background":["#204066",""]}],"primarynav_inactive":["#primary_nav a","Закладка","Оформление закладки в блоке основной навигации",{"background":["#1c3b5f",""],"color":["#c5d5e2",""]}],"primarynav_active":["#primary_nav .active a","Активная закладка","Оформление активной закладки в блоке основной навигации",{"background":["#fff",""],"color":["#0b5794",""]}],"searchbutton":["#search .submit_input","Кнопка поиска","Оформление кнопки поиска",{"background":["#7ba60d",""],"border":["#7ba60d",""]}],"general_box":[".general_box","Контейнер главного блока","Оформление контейнера для главных блоков, такие как используются на странице профиля",{"background":["#fcfcfc",""]}],"general_box_h3":[".general_box h3","H3 в контейнере главного блока","Оформление заголовков в контейнерах главных блоков",{"background":["#DBE2EC",""],"color":["#204066",""]}],"ipsboxcontainer":[".ipsBox_container","Контейнер блока контента","Оформление контейнера в котором содержится блок контента",{"background":["#fff",""],"border":["#dbe4ef",""]}],"maintitle":[".maintitle","Заголовок","Плашка используемая для оформления названий блоков, фильтров и т.п.",{"background":["#2c5687",""],"color":["#fff",""],"border":["#528cbc",""],"boxshadow":["#528cbc"," inset 0px 1px 0 "]}],"desc":[".desc, .desc.blend_links a, p.posted_info","Текст описания","Оформление текстов описания",{"color":["#777777",""]}],"desclighter":[".desc.lighter, .desc.lighter.blend_links a","Текст описания (альтернативный)","Более светлое оформление текстов описания",{"color":["#a4a4a4",""]}],"ipb_table":["table.ipb_table td","Ряды таблиц","Оформление рядов таблиц",{"border":["#f3f3f3",""]}],"sideMenuActive":[".ipsSideMenu ul li.active a","Активный пункт бокового меню","Оформление активного пункта в боковых меню",{"background":["#af286d",""],"color":["#fff",""]}],"ipsVerticalTabbed_tabs_li":[".ipsVerticalTabbed_tabs li","Вертикальная закладка","Оформление боковых вертикальных закладок (например, профиль и панель управления пользователя)",{"background":["#f6f8fb",""],"color":["#808080",""],"border":["#DBE4EF",""]}],"ipsVerticalTabbed_tabs_li_a":[".ipsVerticalTabbed_tabs li a","Ссылка в вертикальной закладке","Оформление ссылки в вертикальной закладке",{"color":["#8d8d8d",""]}],"ipsVerticalTabbed_tabs_li_a_hover":[".ipsVerticalTabbed_tabs li a:hover","Ссылка в вертикальной закладке при наведении курсора мыши","Оформление ссылки в вертикальной закладке при наведении курсора мыши",{"background":["#eaeff5",""],"color":["#808080",""]}],"ipsVerticalTabbed_tabs_li_active":[".ipsVerticalTabbed_tabs li.active a","Активная вертикальная закладка","Оформление активной вертикальной закладки",{"background":["#fff",""],"color":["#353535",""],"border":["#fff",""]}],"ipsfilterbar_item":[".ipsFilterbar li a","Элемент фильтра","Оформление закладок, расположенных на плашке фильтра",{"color":["#fff",""]}],"ipsfilterbar_active":[".ipsFilterbar li.active a","Активный элемент фильтра","Оформление активных закладок, расположенных на плашке фильтра",{"background":["#244156",""],"color":["#fff",""]}],"but_topic":[".topic_buttons li.important a, .topic_buttons li.important span, .ipsButton .important,.topic_buttons li a, .topic_buttons li span, .ipsButton","Основная кнопка","Оформление для большинства кнопок, например кнопки Создать новую тему",{"background":["#212121",""],"border":["#212121",""],"color":["#fff",""],"boxshadow":["#5c5c5c"," inset 0 1px 0 0"]}],"but_secondary":[".ipsButton_secondary","Вторичные кнопки","Оформление остальных кнопок, например кнопок Следить",{"background":["#f6f6f6",""],"border":["#dbdbdb",""],"color":["#616161",""]}],"paginationBackForward":[".pagination .back a,.pagination .forward a","Вперед\/Назад в блоке пагинации","Оформление кнопок перехода в навигации",{"background":["#eaeaea",""],"color":["#5a5a5a",""]}],"paginationBackForwardHover":[".pagination .back a:hover,\t.pagination .forward a:hover","Вперед\/Назад при наведении курсора мыши в блоке пагинации","Оформление кнопок перехода в навигации при наведении курсора мыши",{"background":["#af286d",""],"color":["#fff",""]}],"paginationBackActive":[".pagination .pages li.active","Активная страница в блоке пагинации","Оформление элемента активной страницы",{"background":["#7BA60D",""],"color":["#fff",""]}],"ipsSideBlock":[".ipsSideBlock","Боковой блок ","Оформление боковых блоков",{"background":["#F7FBFC",""]}],"ipsSideBlock_h3":[".ipsSideBlock h3","Заголовок бокового блока","Оформление заголовков боковых блоков",{"background":["#DBE2EC",""],"color":["#204066",""]}],"statusupdate":[".status_update","Блок обновления статуса","Оформление блока обновлений статуса",{"background":["#71a5c9",""],"color":["#fff",""]}],"entry_header":[".entry.featured .entry_header, .entry.featured .entry_footer","Заголовок записи блока","Оформление заголовка для записи блога",{"background":["#f5faf7",""]}],"albumDetailTable_td":["#albumDetailTable td","Рамка таблицы деталей альбома","Оформление рамки таблицы деталей альбома",{"border":["#ebf0f3",""]}]}}
SKINGENRUS;
				
		if ( $json )
		{
			$cacheFile = IPS_CACHE_PATH . 'cache/skinGenJsonCache.js';
			
			if ( file_exists( $cacheFile ) )
			{
				@unlink( $cacheFile );
			}
			
			file_put_contents( $cacheFile, $json );
			@chmod( $cacheFile, 0777 );
		}
		else
		{
			throw new Exception( "JSON_NOT_RETURNED");
		}
		
		return true;
	}
	
	/**
	 * Saves the skin and that
	 * @param	array	css, storedSettings, storedClasses
	 * @param	int		Member Id
	 */
	public function save( array $data, $memberId )
	{
		$session = $this->getUserSession( $memberId );
		
		if ( empty( $session['sg_session_id'] ) )
		{
			throw new Exception( 'NO_SESSION_FOUND' );
		}
		
		/* Update session */
		$this->DB->update( 'skin_collections', array( 'set_skin_gen_data' => serialize( $data ) ), 'set_id=\'' . $session['sg_skin_set_id'] . '\''  );
		
		/* Write CSS_EXISTS extras css */
		try
		{
			$this->saveCSSFromAdd( $session['sg_skin_set_id'], trim( $data['css'] ), 'ipb_skingen', 999, '', 'core' );
		}
		catch( Exception $e )
		{
			if ( $e->getMessage() == 'CSS_EXISTS' )
			{
				$css = $this->fetchCSS( $session['sg_skin_set_id'] );
				$this->saveCSSFromEdit( $css['ipb_skingen']['css_id'], $session['sg_skin_set_id'], trim( $data['css'] ), 'ipb_skingen', 999, '', 'core' );
			}
		}
		
		/* Update replacements */
		$replacements = $this->fetchReplacements( $session['sg_skin_set_id'] );
		
		/* Set transparent logo */
		$this->saveReplacementFromEdit( $replacements['logo_img']['replacement_id'], $session['sg_skin_set_id'], '{style_image_url}/logo_transparent.png', 'logo_img' );
		
		/* Flag for rebuild */
		$this->flagSetForRecache( $session['sg_skin_set_id'] );
		
		/* Delete session */
		$this->deleteUserSession( $memberId );
	}
	
	/**
	 * Resets the member's skin
	 * @param	int		Member Id
	 */
	public function resetMemberAndSwitchSkin( $memberId )
	{
		$session = $this->getUserSession( $memberId );
		
		if ( empty( $session['sg_session_id'] ) )
		{
			throw new Exception( 'NO_SESSION_FOUND' );
		}
		
		IPSMember::save( $memberId, array( 'core' => array( 'bw_using_skin_gen' => 0, 'skin' => $session['sg_skin_set_id'] ) ) );
	}
	
	/**
	 * Set a user's session
	 * @param int $memberId
	 * @param array 
	 */
	public function setUserSession( $memberId, $data )
	{
		if ( ! empty( $memberId ) AND ! empty( $data['skin_set_id'] ) )
		{
			$sessionKey = md5( uniqid() );
			
			$this->deleteUserSession( $memberId );
			
			if ( empty( $data['set_skin_gen_data'] ) )
			{
				$skin = $this->fetchSkinData( $data['skin_set_id'] );
				
				$data['set_skin_gen_data'] = $skin['set_skin_gen_data'];
			}
			
			$this->DB->insert( 'skin_generator_sessions', array( 'sg_session_id'  => $sessionKey,
																 'sg_member_id'   => $memberId,
																 'sg_skin_set_id' => $data['skin_set_id'],
																 'sg_date_start'  => IPS_UNIX_TIME_NOW,
																 'sg_data'		  => ( is_array( $data ) ) ? serialize( $data ) : $data  ) );
			
			/* Flag user */
			IPSMember::save( $memberId, array( 'core' => array( 'bw_using_skin_gen' => 1 ) ) );
			
			return $sessionKey;
		}
		else
		{		
			return false;
		}
	}
	
	/**
	 * Delete a user's session
	 * @param int $memberId
	 * @param null 
	 */
	public function deleteUserSession( $memberId )
	{
		$this->DB->delete( 'skin_generator_sessions', 'sg_member_id=' . intval( $memberId ) );
		
		/* Flag user */
		IPSMember::save( $memberId, array( 'core' => array( 'bw_using_skin_gen' => 0 ) ) );
	}
	
	/**
	 * Get a user's session
	 * @param int $memberId
	 */
	public function getUserSession( $memberId )
	{
		$session = $this->DB->buildAndFetch( array( 'select' => '*',
													'from'   => 'skin_generator_sessions',
													'where'  => 'sg_member_id=' . intval( $memberId ) ) );
		
		if ( ! empty( $session['sg_session_id'] ) )
		{
			if ( IPSLib::isSerialized( $session['sg_data'] ) )
			{
				$session['sg_data_array'] = unserialize( $session['sg_data'] );
				
				if ( IPSLib::isSerialized( $session['sg_data_array']['set_skin_gen_data'] ) )
				{
					$session['skin_gen_data'] = unserialize( $session['sg_data_array']['set_skin_gen_data'] );
				}
			}
				
			return $session;
		}
		else
		{
			/* Prevent this from loading again */
			IPSMember::save( $memberId, array( 'core' => array( 'bw_using_skin_gen' => 0 ) ) );
			
			return false;
		}
	}
	
	/**
	 * Get a user's session
	 * @param int $skinSetId
	 */
	public function getSessionBySkinId( $skinSetId )
	{
		$session = $this->DB->buildAndFetch( array( 'select' => '*',
													'from'   => 'skin_generator_sessions',
													'where'  => 'sg_skin_set_id=' . intval( $skinSetId ) ) );
		
		if ( ! empty( $session['sg_session_id'] ) )
		{
			if ( IPSLib::isSerialized( $session['sg_data'] ) )
			{
				$session['sg_data_array'] = unserialize( $session['sg_data'] );
				
				if ( IPSLib::isSerialized( $session['sg_data_array']['set_skin_gen_data'] ) )
				{
					$session['skin_gen_data'] = unserialize( $session['sg_data_array']['set_skin_gen_data'] );
				}
			}
				
			return $session;
		}
		else
		{
			return false;
		}
	}
	
	
}