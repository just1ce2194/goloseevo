<?php
/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.3.4
 * Virus scanner: known blacklisted filenames
 * Last Updated: $Date: 2012-06-12 10:14:49 -0400 (Tue, 12 Jun 2012) $
 * </pre>
 *
 * @author 		$Author: bfarber $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		Nulled by IPBZona.ru
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		Tue. 17th August 2004
 * @version		$Rev: 10914 $
 *
 */

$KNOWN_NAMES = array(
'lang_choise.php',
'00.php',
'd6.php',
'r57.php',
'skin_admin.php',
'test.php3',
'1.php',
'2.php',
'.php',
'mysql.php',
'shell.php',
'c99.php',
'info.php',
'skin_msq.php',
'lang_msq.php',
'skin_posts.php',
'lang_posts.php',
);
