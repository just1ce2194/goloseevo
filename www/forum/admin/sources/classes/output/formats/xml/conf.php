<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.3.4
 * Ouput format: XML
 * (Matt Mecham)
 * Last Updated: $Date: 2012-06-12 10:14:49 -0400 (Tue, 12 Jun 2012) $
 * </pre>
 *
 * @author    $Author: bfarber $
 * @copyright (c) 2001 - 2009 Invision Power Services, Inc.
 * @license   Nulled by IPBZona.ru
 * @package   IP.Board
 * @link    http://www.invisionpower.com
 * @since   9th March 2005 11:03
 * @version   $Revision: 10914 $
 *
 */

$config = array();

/* Identifies publicly as... */
$config['identifies_as'] = 'XML';

/* Gateway file? */
$config['gateway_file']  = 'xml.php';