<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.3.4
 * URL shortener
 * Owner: Matt Mecham
 * Last Updated: $Date: 2012-06-12 10:14:49 -0400 (Tue, 12 Jun 2012) $
 * </pre>
 *
 * @author 		Matt Mecham
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		Nulled by IPBZona.ru
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		24th November 2009
 * @version		$Revision: 10914 $
 */

$config = array();

/* Login... */
$config['login'] = '';

/* API KEY - Not needed : */
$config['apiKey']  = '';

