<?php
/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.3.4
 * Update captcha image
 * Last Updated: $Date: 2012-06-12 10:14:49 -0400 (Tue, 12 Jun 2012) $
 * </pre>
 *
 * @author 		$Author: bfarber $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		Nulled by IPBZona.ru
 * @package		IP.Board
 * @subpackage	Core
 * @link		http://www.invisionpower.com
 * @since		2.3
 * @version		$Revision: 10914 $
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class public_core_ajax_captcha extends ipsAjaxCommand 
{
	/**
	 * Main class entry point
	 *
	 * @param	object		ipsRegistry reference
	 * @return	@e void		[Outputs to screen]
	 */
	public function doExecute( ipsRegistry $registry )
	{
    	switch( ipsRegistry::$request['do'] )
    	{
			default:
			case 'refresh':
    			$this->refresh();
    		break;
    		
    	}
	}
	
	/**
	 * Refresh the captcha image
	 *
	 * @return	@e void		[Outputs to screen]
	 */
	public function refresh()
	{
		$captcha_unique_id = trim( IPSText::alphanumericalClean( ipsRegistry::$request['captcha_unique_id'] ) );
		
		$template    = $this->registry->getClass('class_captcha')->getTemplate( $captcha_unique_id );
		$newUniqueID = $this->registry->getClass('class_captcha')->captchaKey;

		$this->returnString( $newUniqueID );
	}

}