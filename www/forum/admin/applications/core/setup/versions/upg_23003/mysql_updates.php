<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.3.4
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2004 Invision Power Services
|   http://www.invisionpower.com
|   ========================================
|   Web: http://www.invisionpower.com
|   Email: matt@invisionpower.com
|   Licence: Nulled by IPBZona.ru
+---------------------------------------------------------------------------
*/


$SQL[] = "ALTER TABLE skin_sets ADD set_key VARCHAR( 32 ) NULL ;";
$SQL[] = "ALTER TABLE skin_sets ADD INDEX ( set_key ) ;";

