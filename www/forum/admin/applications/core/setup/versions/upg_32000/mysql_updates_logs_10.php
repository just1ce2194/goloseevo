<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.3.4
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2009 Invision Power Services
|   http://www.invisionpower.com
|   ========================================
|   Web: http://www.invisionpower.com
|   Email: matt@invisionpower.com
|   Licence: Nulled by IPBZona.ru
+---------------------------------------------------------------------------
*/

$PRE = trim(ipsRegistry::dbFunctions()->getPrefix());
$DB  = ipsRegistry::DB();


$TABLE	= 'task_logs';
$SQL[]	= "ALTER TABLE task_logs CHANGE log_ip log_ip VARCHAR( 46 ) NOT NULL DEFAULT '0';";


