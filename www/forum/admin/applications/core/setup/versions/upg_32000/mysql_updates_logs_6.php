<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.3.4
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2009 Invision Power Services
|   http://www.invisionpower.com
|   ========================================
|   Web: http://www.invisionpower.com
|   Email: matt@invisionpower.com
|   Licence: Nulled by IPBZona.ru
+---------------------------------------------------------------------------
*/

$PRE = trim(ipsRegistry::dbFunctions()->getPrefix());
$DB  = ipsRegistry::DB();

$TABLE	= 'dnames_change';
$SQL[]	= "ALTER TABLE dnames_change CHANGE dname_ip_address dname_ip_address VARCHAR( 46 ) NOT NULL;";


