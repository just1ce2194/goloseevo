<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.3.4
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2004 Invision Power Services
|   http://www.invisionpower.com
|   ========================================
|   Web: http://www.invisionpower.com
|   Email: matt@invisionpower.com
|   Licence: Nulled by IPBZona.ru
+---------------------------------------------------------------------------
*/


$SQL[] = "ALTER TABLE members CHANGE email email varchar( 150 ) NOT NULL default ''";

$SQL[] = "ALTER TABLE subscription_currency CHANGE `subcurrency_exchange` `subcurrency_exchange` DECIMAL( 16, 8 ) DEFAULT '0.00000000' NOT NULL";

