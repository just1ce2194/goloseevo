<?php
/**
 * @file		defaultSection.php 	Define the default section for the 'archive' module
 *~TERABYTE_DOC_READY~
 * $Copyright: (c) 2001 - 2011 Invision Power Services, Inc.$
 * $License: Nulled by IPBZona.ru
 * $Author: ips_terabyte $
 * @since		Tuesday 17th August 2004
 * $LastChangedDate: 2010-10-14 13:11:17 -0400 (Thu, 14 Oct 2010) $
 * @version		v3.3.4
 * $Revision: 477 $
 */

$DEFAULT_SECTION = 'archive';
