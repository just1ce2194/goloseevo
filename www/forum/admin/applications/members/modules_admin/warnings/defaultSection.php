<?php
/**
 * @file		defaultSection.php 	Define the default section for the 'warnings' module
 *~TERABYTE_DOC_READY~
 * $Copyright: (c) 2001 - 2011 Invision Power Services, Inc.$
 * $License: Nulled by IPBZona.ru
 * $Author: bfarber $
 * @since		07th May 2012
 * $LastChangedDate: 2012-06-12 10:14:49 -0400 (Tue, 12 Jun 2012) $
 * @version		v3.3.4
 * $Revision: 10914 $
 */

$DEFAULT_SECTION = 'reasons';
