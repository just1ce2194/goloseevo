<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.3.4
 * Installation Gateway
 * Last Updated: $LastChangedDate: 2012-06-12 10:14:49 -0400 (Tue, 12 Jun 2012) $
 * </pre>
 *
 * @author 		$Author: bfarber $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		Nulled by IPBZona.ru
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		14th May 2003
 * @version		$Rev: 10914 $
 */

define( 'IPB_THIS_SCRIPT', 'admin' );
define( 'IPS_IS_UPGRADER', FALSE );
define( 'IPS_IS_INSTALLER', TRUE );

require_once( '../../initdata.php' );/*noLibHook*/

$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

/**
* Are we overwriting an existing IP.Board 2 installation?
*/
if ( is_file( DOC_IPS_ROOT_PATH . 'sources/ipsclass.php' ) )
{
	@header( "Location: http://" . $_SERVER["SERVER_NAME"] . str_replace( "/install/", "/upgrade/", $_SERVER["PHP_SELF"] ) );
	exit();
}

require_once( IPS_ROOT_PATH . 'setup/sources/base/ipsRegistry_setup.php' );/*noLibHook*/
require_once( IPS_ROOT_PATH . 'setup/sources/base/ipsController_setup.php' );/*noLibHook*/

ipsController::run();

exit();

?>