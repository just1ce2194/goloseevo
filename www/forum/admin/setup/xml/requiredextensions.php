<?php
/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.3.4
 * Some required extensions to check for
 * Last Updated: $Date: 2012-06-12 10:14:49 -0400 (Tue, 12 Jun 2012) $
 * </pre>
 *
 * @author 		Matt Mecham
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		Nulled by IPBZona.ru
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		1st December 2008
 * @version		$Revision: 10914 $
 *
 */
 
$INSTALLDATA = array(
	
array( 'prettyname'		=> "Поддержка DOM XML",
	   'extensionname'	=> "libxml2",
	   'helpurl'		=> "http://www.php.net/manual/en/dom.setup.php",
	   'testfor'		=> 'dom',
	   'nohault'		=> false ),

array( 'prettyname'		=> "Библиотека GD",
	   'extensionname'	=> "gd",
	   'helpurl'		=> "http://www.php.net/manual/en/image.setup.php",
	   'testfor'		=> 'gd',
	   'nohault'		=> true ),
	
	
array( 'prettyname'		=> "Класс Reflection",
	   'extensionname'	=> "Reflection",
	   'helpurl'		=> "http://www.php.net/manual/en/language.oop5.reflection.php",
	   'testfor'		=> 'Reflection',
	   'nohault'		=> false ),

array( 'prettyname'		=> "SPL",
	   'extensionname'	=> "SPL",
	   'helpurl'		=> "http://www.php.net/manual/en/book.spl.php",
	   'testfor'		=> 'SPL',
	   'nohault'		=> true ),
	   
array( 'prettyname'		=> "OpenSSL",
	   'extensionname'	=> "openssl",
	   'helpurl'		=> "http://www.php.net/manual/en/book.openssl.php",
	   'testfor'		=> 'openssl',
	   'nohault'		=> true ),
	   
array( 'prettyname'		=> "JSON",
	   'extensionname'	=> "json",
	   'helpurl'		=> "http://www.php.net/manual/en/book.json.php",
	   'testfor'		=> 'json',
	   'nohault'		=> true ),
);