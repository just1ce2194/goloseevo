<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('INSERT INTO unchecked_requests (name, phone, email, street, building, message, request_date) VALUES (?,?,?,
                          (SELECT street_id FROM streets s WHERE s.name=?),
                          (SELECT building_id FROM buildings b WHERE b.number=?),
                          ?,?)')) {
    $stmt->bind_param('sssssss',$jsonArray['name'], $jsonArray['phone'], $jsonArray['email'], $jsonArray['street'], $jsonArray['building'], $jsonArray['message'],$jsonArray['date']);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
echo "SUCCESS";
$db->close();