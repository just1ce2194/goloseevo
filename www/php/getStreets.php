<?php
header('Content-Type: text/html; charset=utf-8');
include("db.php");
$result = mysqli_query($db,"SELECT name FROM streets");
$db->close();
$return_arr = array();
while ($row = mysqli_fetch_array($result)) {
    $row_array['NAME'] = $row[0];
    array_push($return_arr,$row_array);
}
echo json_encode($return_arr);