<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if (strcasecmp($jsonArray['type'],"planned")==0) {
    if ($stmt= $db->prepare('SELECT n.title, i.link, n.news_text, n.news_date FROM planned_news n, images i
                  WHERE n.image=i.image_id and n.id=?')){
        $stmt->bind_param('d',$jsonArray['id']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->bind_result($title, $image, $text, $date);
        $res = array();
        while ($stmt->fetch()) {
            $r = array();
            $r['title'] = $title;
            $r['image'] = $image;
            $r['text'] = $text;
            $r['date'] = $date;
            array_push($res, $r);
        }
        $stmt->close();
    }
    $db->close();
    echo json_encode($res);
}
else if (strcasecmp($jsonArray['type'],"done")==0) {
    if ($stmt= $db->prepare('SELECT n.title, i.link, n.news_text, n.news_date FROM done_news n, images i
                  WHERE n.image=i.image_id and n.id=?')){
        $stmt->bind_param('d',$jsonArray['id']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->bind_result($title, $image, $text, $date);
        $res = array();
        while ($stmt->fetch()) {
            $r = array();
            $r['title'] = $title;
            $r['image'] = $image;
            $r['text'] = $text;
            $r['date'] = $date;
            array_push($res, $r);
        }
        $stmt->close();
    }
    $db->close();
    echo json_encode($res);
}
else if (strcasecmp($jsonArray['type'],"actual")==0) {
    if ($stmt= $db->prepare('SELECT n.title, i.link, n.news_text, n.news_date FROM actual_news n, images i
                  WHERE n.image=i.image_id and n.id=?')){
        $stmt->bind_param('d',$jsonArray['id']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->bind_result($title, $image, $text, $date);
        $res = array();
        while ($stmt->fetch()) {
            $r = array();
            $r['title'] = $title;
            $r['image'] = $image;
            $r['text'] = $text;
            $r['date'] = $date;
            array_push($res, $r);
        }
        $stmt->close();
    }
    $db->close();
    echo json_encode($res);
}
