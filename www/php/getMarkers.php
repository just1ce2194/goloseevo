<?php
$str_json = file_get_contents('php://input');
include("db.php");
$query = sprintf("SELECT m.description, m.lat, m.lng, i.LINK FROM markers m, images i where m.imageid=i.IMAGE_ID");
$result = mysqli_query($db, $query) or die('Query failed: ' . mysql_error());;
$db->close();
$return_arr = array();
while ($row = mysqli_fetch_array($result)) {
    $row_array['description'] = $row[0];
    $row_array['lat'] = $row[1];
    $row_array['lng'] = $row[2];
    $row_array['link'] = $row[3];
    array_push($return_arr,$row_array);
}
echo json_encode($return_arr);