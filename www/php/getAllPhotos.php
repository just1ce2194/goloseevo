<?php
include("db.php");
if ($stmt = $db->prepare('SELECT p.link, p.description, a.name
                          FROM photos p, albums a
                          WHERE p.album=a.album_id')) {
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->bind_result($link, $desc, $name);
    $res = array();
    while ($stmt->fetch()) {
        $r = array();
        $r['link'] = $link;
        $r['description'] = $desc;
        $r['album'] = $name;
        array_push($res, $r);
    }
    $stmt->close();
}
$db->close();
echo json_encode($res);