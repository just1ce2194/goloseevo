function addPolygon() {
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Додати будинок</h1>'));
    wrapper.append($('<label>Вулиця: </label><select id="street"></select><br>'));
    wrapper.append($('<input id="building" type="text" placeholder="Building"/><br><input id="center" ' +
        'type="text" placeholder="Center"/><br><input name="coords[]" type="text" placeholder="Coordinate"/>' +
        '<br><input name="coords[]" type="text" placeholder="Coordinate"/><br><input name="coords[]" type="text" ' +
        'placeholder="Coordinate"/><br><input name="coords[]" type="text" placeholder="Coordinate"/><br>'));
    wrapper.append($('<div id="div"></div><input id="addButton" type="button" ' +
        'value="Add coordinate" onclick="addCoordinate()"/><input id="removeButton" type="button" ' +
        'value="Remove coordinate" onclick="removeCoordinate()"/><br><input id="picture" type="text" ' +
        'placeholder="Picture"/><br>'+
        '<input type="checkbox" id="check" onclick="checkInfo(this)">Заповнити інформацію про будинок вручну<br>'+
        '<div id="autoInfo">'+
        '<label>Кількість під\'їздів: </label><input id="countEntrances"/><br>'+
        '<label>Кількість квартир: </label><input id="countApartments"/><br>'+
        '<label>Кількість мешканців: </label><input id="countResidents"/><br></div>'+
        '<input id="link" type="text" placeholder="Link to forum"/><br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append('<input type="submit" onclick="saveBuildingToDB()"/>');
    $.getScript("streets.js", loadStreetsToSelect());
}

function checkInfo(element){
    if (element.checked) {
        $('#autoInfo').empty();
        $('#autoInfo').append($('<textarea id="info" rows="5" cols="50" placeholder="Info"/><br>'));
    }
    else{
        $('#autoInfo').empty();
        $('#autoInfo').append($('<label>Кількість під\'їздів: </label><input id="countEntrances"/><br>'+
        '<label>Кількість квартир: </label><input id="countApartments"/><br>'+
        '<label>Кількість мешканців: </label><input id="countResidents"/><br></div>'));
    }
}

function addCoordinate(){
    $('<input name="coords[]" type="text" placeholder="Coordinate"/><br>')
        .appendTo('#div');
}

function removeCoordinate(){
    if ($('#div').children().length>0) {
        $('[name="coords[]"]')[$('[name="coords[]"]').size()-1].remove();
        $('#div br').last().remove();
    }
}

function saveBuildingToDB(){
    var street = $("#street option:selected").text();
    var building = $('#building').val();
    var coords = $('[name="coords[]"]');
    var newCoords = "";
    for (var i = 0; i < coords.length; i++) {
        newCoords+=coords[i].value+"; ";
    }
    var center = $('#center').val();
    var picture = $('#picture').val();
    var link = $('#link').val();

    var info;
    var check = $('#check');
    if (!check.prop('checked')){
        info = "Під\'їздів - "+$('#countEntrances').val()+"<br>Квартир - "+$('#countApartments').val();
        if ($('#countResidents').val()!="" && $('#countResidents').val()!="0") {
            info = info+"<br>Мешканців - " + $('#countResidents').val();
        }
    }
    else{
        info = $('#info').val().replace(/\n/g,'<br>');
    }

    var content = new Object();
    content.street = street;
    content.number = building;
    content.center = center;
    content.coordinates = newCoords;
    content.picture = picture;
    content.info = info;
    content.linktoforum = link;

    var req = new XMLHttpRequest();
    req.open("POST", "includes/saveBuildingToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));

    if (req.responseText==="SUCCESS"){
        $('#message').css({"color":"blue"});
        $('#message').html('Будинок був добавлений в базу даних.');
        $('#building').val('');
        $('[name="coords[]"]').val('');
        $('#center').val('');
        $('#picture').val('');
        $('#info').val('');
        $('#link').val('');

    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}

function editPolygons(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Редагувати будинок</h1>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<div id="divtable"></div>'))
    var req = new XMLHttpRequest();
    req.open("POST", "includes/getPolygons.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(null);

    var res = JSON.parse(req.responseText);
    if (res.length===0){
        $('#message').css({"color":"red"});
        $('#message').html("База будинків пуста.");
        return;
    }
    var divtable = $('#divtable');
    divtable.empty();
    var table = $('<table></table>');
    table.append($('<tr><th>№</th><th>Вулиця</th><th>Номер будинку</th><th>Координати центра</th><th>Координати</th><th>Картинка</th><th>Інформація</th><th>Посилання на форум</th></tr>'));
    for (var i=0;i<res.length;i++){
        var tr = $('<tr></tr>');
        tr.append($('<td hidden id="id'+i+'">'+res[i]['id']+'</td>'));
        tr.append($('<td>'+(i+1)+'</td>'));
        tr.append($('<td id="street'+i+'">'+res[i]['street']+'</td>'));
        tr.append($('<td id="building'+i+'">'+res[i]['number']+'</td>'));
        tr.append($('<td><input style="width: 130px" id="center'+i+'" value="'+res[i]['center']+'"></td>'));
        var coordinates = res[i]['coordinates'].split('; ');
        var text="";
        for (var j=0;j<coordinates.length-1;j++){
            text+=coordinates[j]+"\n";
        }
        tr.append($('<td><textarea rows="'+(coordinates.length)+'" cols="20" style="resize: none" id="coordinates'+i+'">'+text+'</textarea></td>'));
        tr.append($('<td><input id="picture'+i+'" value="'+res[i]['picture']+'"></td>'));
        tr.append($('<td><textarea rows="5" cols="20" style="resize:none" id="info'+i+'">'+res[i]['info']+'</textarea></td>'));
        tr.append($('<td><textarea rows="5" cols="20" style="resize:none" id="linktoforum'+i+'">'+res[i]['linktoforum']+'</textarea></td>'));
        tr.append($('<td><button id="edit'+i+'" onclick="editBuilding(this)">Edit</button><br><button id="delete'+i+'" onclick="deleteBuilding(this)">Delete</button> </td>'))
        table.append(tr);
    }
    divtable.append(table);
    divtable.append($('<p id="message"></p>'));
}

function editBuilding(e){
    var i = e.id.replace('edit','');
    var id =$('#id'+i).text();
    var building = $('#building'+i).text();
    var center = $('#center'+i).val();
    var crds = $('#coordinates'+i).val();
    var coords = crds.split('\n');
    var coordinates = "";
    for (var j=0;j<coords.length-1;j++){
        coordinates+=coords[j]+"; ";
    }
    var picture = $('#picture'+i).val();
    var info = $('#info'+i).val();
    var linktoforum = $('#linktoforum'+i).val();

    var content = new Object();
    content.id = id;
    content.building = building;
    content.center = center;
    content.coordinates = coordinates;
    content.picture = picture;
    content.info = info;
    content.linktoforum = linktoforum;

    var request = new XMLHttpRequest();
    request.open("POST", "includes/updateBuildingToDB.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==="SUCCESS"){
        alert("Інформація про будинок \""+building+"\" була змінена.");
        editPolygons();
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+request.responseText);
    }
}

function deleteBuilding(e){
    var i = e.id.replace('delete','');
    var id = $('#id'+i).text();
    var building = $('#building'+i).text();
    var request = new XMLHttpRequest();
    request.open("POST", "includes/deleteBuildingFromDB.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(id));

    if (request.responseText==="SUCCESS"){
        alert("Будинок \""+building+"\" був видалений з бази даних.");
        editPolygons();
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+request.responseText);
    }
}