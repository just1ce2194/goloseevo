function uploadImage(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Завантажити картинку</h1>'));
    wrapper.append($('<form action="includes/uploadImage.php" method="post" enctype="multipart/form-data"> ' +
        '<label>Тип картинки(для новин "newsIcons", для маркерів "icons"): </label><br><select name="imageType" id="type"></select><br>'+
        '<input type="file" name="filename" ><br>' +
        '<input type="submit" value="Завантажити"><br>' +
        '</form>'));
    loadTypesToSelect();
}
function loadTypesToSelect(){
    var sel = $('#type');
    var req = new XMLHttpRequest();
    req.open("POST", "includes/getImageTypes.php", false);
    req.setRequestHeader("Content-type", "text/html");
    req.send(null);
    var res = JSON.parse(req.responseText);
    for (var i=0;i<res.length;i++){
        sel.append($('<option>'+res[i]['type']+'</option>'))
    }
}

function deleteImage(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Видалити картинку</h1>'));
    wrapper.append($('<form action="includes/deleteImage.php" method="post" enctype="multipart/form-data"> ' +
        '<label>Тип картинки(іконки): </label><select id="type" onchange="$(\'#del\').remove();loadImages();"><option id="del"></option></select><br>'+
        '<label>Картинка(іконка): </label><select name="image" id="image" onchange="showImage()"></select><br>'+
        '<div id="imgplace"></div><br>'+
        '<input type="submit" value="Видалити"><br>' +
        '</form>'));
    loadTypesToSelect();
}

function loadImages(){
    var wrapper = $('#page-wrapper');
    var sel = $('#image');
    sel.empty();
    var type = $('#type option:selected').val();
    var req = new XMLHttpRequest();
    req.open("POST", "includes/getImages.php", false);
    req.setRequestHeader("Content-type", "application/json");
    var content = new Object();
    content.type = type;
    req.send(JSON.stringify(content));
    var res = JSON.parse(req.responseText);
    for (var i=0;i<res.length;i++){
        sel.append($('<option>'+res[i]['LINK']+'</option>'))
    }
}

function showImage(){
    $('#imgplace').html('<img src="'+'/'+$('#image option:selected').text().substring($('#image option:selected').text().indexOf('images'))+'" width="100px"/>');
}