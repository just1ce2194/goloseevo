function addMarker(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Додати маркер</h1>'));
    wrapper.append($('<label>Координати:</label><br><input id="lat" type="text"/><br><input id="lng" type="text" /><br>'+
        '<label>Картинка:</label><br><select id="picture" onchange="onChange()"><option value="" id="delete"></option></select>' +
        '<div id="imgplace"></div><br><label>Опис маркеру:</label><br><textarea rows="9" cols="140" id="textarea" ' +
        'required data-validation-required-message="Опис маркеру" maxlength="10000" style="resize:none"></textarea><br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<button id="button" onclick="saveMarkerToDB()">Додати Маркер</button>'));
    getImages('icons');
}
function deleteMarker(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Видалити маркер</h1>'));
    wrapper.append($('<label>Iдентифiкатор:</label><br><select id="selectID" onchange="onChangeId()"><option value="" id="delete"></option></select>' +
        '<div id="imgplace"></div><br><label>Опис маркеру:</label><br><textarea rows="9" cols="140" id="textarea" ' +
        'required data-validation-required-message="Опис маркеру" maxlength="10000" style="resize:none"></textarea><br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<button id="button" onclick="deleteMarkerFromDB()">Видалати Маркер</button>'));
    getMarkerIDs();
}
var typeNews;

function onChange(){
    $('#delete').remove();
    $('#imgplace').html('<img src="'+$('#picture option:selected').text()+'" width="100px"/>');
}
function onChangeId(){
    $('#textarea').html(getMarkerDescByID($('#selectID option:selected').val()));
}
function getMarkerDescByID(id){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getMarkerDescByID.php", false);
    request.setRequestHeader("Content-type", "application/json");
    var content = new Object();
    content.id = id;
    request.send(JSON.stringify(content));
    if (request.responseText.length===0){
        $('#message').css({"color":"red"});
        $('#message').html('Помилка!');
        return;
    }

    var res = JSON.parse(request.responseText);
    return res[0]['DESC'];
}
function getMarkerIDs(){
    $('#selectID').html('');
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getMarkerIDs.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(null);
    if (request.responseText.length===0){
        $('#message').css({"color":"red"});
        $('#message').html('Помилка!');
        return;
    }
    var res = JSON.parse(request.responseText);
    for (var i=0;i<res.length;i++){
        $('#selectID').append($('<option value="'+res[i]['ID']+'">'+res[i]['ID']+'</option>'))
    }
    $('#textarea').val(getMarkerDescByID($('#selectID option:selected').val()));
}
function getImages(type){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getImages.php", false);
    request.setRequestHeader("Content-type", "application/json");
    var content = new Object();
    content.type = type;
    request.send(JSON.stringify(content));
    if (request.responseText.length===0){
        $('#message').css({"color":"red"});
        $('#message').html('Таблиця \"images\" порожня!');
        return;
    }
    var res = JSON.parse(request.responseText);
    for (var i=0;i<res.length;i++){
        $('#picture').append($('<option value="'+res[i]['LINK']+'">'+'/'+res[i]['LINK'].substring(res[i]['LINK'].indexOf('images'))+'</option>'))
    }
}

function saveMarkerToDB(){
    var lat = $('#lat').val();
    var lng = $('#lng').val();
    var image =  $('#picture option:selected').val();
    var desc = $('#textarea').val();

    var content = new Object();
    content.lat = lat;
    content.lng = lng;
    content.imagelink = image;
    content.desc = desc;


    var req = new XMLHttpRequest();
    req.open("POST", "includes/saveMarkerToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));
    if (req.responseText==="SUCCESS"){
        $('#message').css({"color":"blue"});
        $('#message').html('Маркер добавлено!');
        $('#textarea').val('');
        $('#imgplace').val('');
        $('#lat').val('');
        $('#lng').val('');
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}
function deleteMarkerFromDB(){

    var id =  $('#selectID option:selected').val();


    var content = new Object();
    content.id = id;

    var req = new XMLHttpRequest();
    req.open("POST", "includes/deleteMarkerFromDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));
    if (req.responseText==="SUCCESS"){
        $('#message').css({"color":"blue"});
        $('#message').html('Маркер видалено!');
        $('#textarea').val('');
        $('#imgplace').val('');
        getMarkerIDs();
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}

