function addStreet(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Додати вулицю</h1>'));
    wrapper.append($('<input id="name" type="text" placeholder="Name"/><br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<input type="submit" onclick="saveStreetToDB()"/>'));
}

function saveStreetToDB(){
    var name = $('#name').val();

    var req = new XMLHttpRequest();
    req.open("POST", "includes/saveStreetToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(name));

    if (req.responseText==="SUCCESS"){
        $('#message').css({"color":"blue"});
        $('#message').html("Вулиця \""+name+"\" була добавлена в базу даних.");
        $('#name').val('');
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}

function loadStreets(){


    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Редагувати вулицю</h1>'));
    wrapper.append($('<select id="street"></select>'));
    wrapper.append($('<br>'));
    wrapper.append($('<input type="text" id= "newName" placeholder="Нова назва">'));
    wrapper.append($('<br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<button type="button" onclick="updateStreet()">Змінити</button>'));
    wrapper.append($('<button type="button" onclick="deleteStreet()">Видалити</button>'));

    loadStreetsToSelect();
}

function loadStreetsToSelect(){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getStreets.php", false);
    request.send(null);
    if (request.responseText.length===0){
        $('#message').css({"color":"red"});
        $('#message').html('Таблиця \"streets\" порожня!');
        return;
    }
    $('#street').empty();
    var res = JSON.parse(request.responseText);
    for (var i=0;i<res.length;i++){
        $('#street').append($('<option value="'+res[i]['NAME']+'">'+res[i]['NAME']+'</option>'))
    }
}
function updateStreet(){
    var oldName = $('#street option:selected').text();
    var newName = $('#newName').val();
    var content = new Object();
    content.oldName = oldName;
    content.newName = newName;

    var req = new XMLHttpRequest();
    req.open("POST", "includes/updateStreetToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));

    if (req.responseText==="SUCCESS"){
        $('#message').css({"color":"blue"});
        $('#message').html("Вулиця \""+oldName+"\" була оновлена в базі даних.");
        loadStreetsToSelect();
        $('#newName').val("");
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}

function deleteStreet(){
    var name = $('#street option:selected').text();

    var req = new XMLHttpRequest();
    req.open("POST", "includes/deleteStreetFromDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(name));

    if (req.responseText==="SUCCESS"){
        $('#message').css({"color":"blue"});
        $('#message').html("Вулиця \""+name+"\" була видалена з бази даних.");
        loadStreetsToSelect();
        $('#newName').val("");
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}