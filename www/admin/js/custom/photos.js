function addAlbum(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Додати альбом</h1>'));
    wrapper.append($('<label>Назва альбому:</label><br><input id="name" type="text" placeholder="Назва"/><br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<button onclick="createAlbum()">Додати альбом</button>'));
}

function createAlbum(){
    var name = $('#name').val();

    var req = new XMLHttpRequest();
    req.open("POST", "includes/saveAlbumToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(name));

    if (req.responseText==="SUCCESS"){
        $('#message').css({"color":"blue"});
        $('#message').html("Альбом \""+name+"\" був доданий в базу даних.");
        $('#name').val('');
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}

function deleteAlbum(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Видалити альбом</h1>'));
    wrapper.append($('<select id="album"></select>'));
    wrapper.append($('<br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<button type="button" onclick="deleteAlb()">Видалити</button>'));
    $('#album').empty();
    loadAllAlbums();
}

function loadAllAlbums(){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getAlbums.php", false);
    request.send(null);
    if (request.responseText.length===0){
        $('#message').css({"color":"red"});
        $('#message').html('Таблиця \"streets\" порожня!');
        return;
    }
    var res = JSON.parse(request.responseText);
    for (var i=0;i<res.length;i++){
        $('#album').append($('<option value="'+res[i]['NAME']+'">'+res[i]['NAME']+'</option>'))
    }
}

function deleteAlb(){
    if (confirm("Увага!!! При видаленні альбому будуть видалені ВСІ фото з даного альбому без можливого відновлення!!!" +
            "\nНатисніть ОК, якщо хочете видалити альбом і фотографії в ньому.")) {
        var name = $('#album option:selected').text();

        var req = new XMLHttpRequest();
        req.open("POST", "includes/deleteAlbumFromDB.php", false);
        req.setRequestHeader("Content-type", "application/json");
        req.send(JSON.stringify(name));

        if (req.responseText === "SUCCESS") {
            $('#message').css({"color": "blue"});
            $('#message').html("Альбом \"" + name + "\" був видалений з бази даних.");
            $('#album').empty();
            loadAllAlbums();
        }
        else {
            $('#message').css({"color": "red"});
            $('#message').html("Помилка БД!<br>" + req.responseText);
        }
    }
}

function addPhotoToAlbum(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Додати фото в альбом</h1>'));
    wrapper.append($('<form action="includes/uploadPhoto.php" method="post" enctype="multipart/form-data"> ' +
        '<label>Альбом:</label><br><select name="imageType" id="album"></select><br>'+
        '<input type="file" name="filename" ><br>' +
        '<label>Опис:</label><br><input type="text" name="description" id="description"/><br>'+
        '<input type="submit" value="Завантажити"><br>' +
        '</form>'));
    $('#album').empty();
    loadAllAlbums();
}

function deletePhotoFromAlbum(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Видалити фото з альбому</h1>'));
    wrapper.append($('<label>Альбом: </label><br><select id="album" onchange="loadAlbumPhotos()"><option id="deletedAlbum" value=""> </option></select><br>'));
    wrapper.append($('<label>Фото: </label><br><select id="photo" onchange="showAlbumPhoto()"><option id="deletedPhoto" value=""> </option></select>'));
    wrapper.append($('<div id="imgplace"></div><br>'));
    wrapper.append($('<br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<button type="button" onclick="deletePhoto()">Видалити фото</button>'));
    loadAllAlbums();
}

function loadAlbumPhotos(){
    $('#deletedAlbum').remove();
    var name = $('#album option:selected').text();
    var req = new XMLHttpRequest();
    req.open("POST", "includes/getAlbumPhotos.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(name));

    if (req.responseText.length===0){
        $('#message').css({"color":"red"});
        $('#message').html('Таблиця \"streets\" порожня!');
        return;
    }
    $('#photo').empty();
    var res = JSON.parse(req.responseText);
    for (var i=0;i<res.length;i++){
        $('#photo').append($('<option value="'+res[i]['link']+'">'+res[i]['link']+'</option>'))
    }
    if ($('#photo option:selected').text()!="") {
        $('#imgplace').html('<img src="http://goloseevo/' + $('#photo option:selected').text().substring($('#photo option:selected').text().indexOf("images")) + '" width="100px"/>');
    }
}

function showAlbumPhoto(){
    $('#deletedPhoto').remove();
    if ($('#photo option:selected').text()!="") {
        $('#imgplace').html('<img src="http://goloseevo/' + $('#photo option:selected').text().substring($('#photo option:selected').text().indexOf("images")) + ' width="100px"/>');
    }
}

function deletePhoto(){
    var photo = $('#photo option:selected').text();
    var req = new XMLHttpRequest();
    req.open("POST", "includes/deletePhotoFromAlbum.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(photo));

    if (req.responseText === "SUCCESS") {
        $('#message').css({"color": "blue"});
        $('#message').html("Фото було видалене з бази даних.");
        $('#album').empty();
        $('#photo').empty();
        $('#imgplace').empty();
        loadAllAlbums();
        loadAlbumPhotos();
    }
    else {
        $('#message').css({"color": "red"});
        $('#message').html("Помилка БД!<br>" + req.responseText);
    }
}