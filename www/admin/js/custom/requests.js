function requests(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Необроблені заявки</h1>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<div id="divtable"></div>'));
    getRequests();
}

function checkedRequests(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Оброблені заявки</h1>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<div id="divtable"></div>'));
    getCheckedRequests();
}

function getRequests(){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getUncheckedRequests.php", false);
    request.send(null);

    var res = JSON.parse(request.responseText);
    $('#divtable').empty();
    if (res.length!=0) {
        var table = $('<table id="tableid" style="border-collapse: collapse"></table>');
        $('#divtable').append(table);
        $('#tableid').append($('<tr><th style="padding: 3px">№</th>' +
            '<th style="padding: 3px">Ім"я відправника</th>' +
            '<th style="padding: 3px">Телефон</th>' +
            '<th style="padding: 3px">Email</th>' +
            '<th style="padding: 3px">Дата</th>' +
            '<th style="padding: 3px">Вулиця</th>' +
            '<th style="padding: 3px">Будинок</th>' +
            '<th style="padding: 3px">Повідомлення</th>' +
            '<th style="padding: 3px">Заголовок</th></tr>'));
        for (var i = 0; i < res.length; i++) {
            $('#tableid').append($('<tr id="tr' + i + '"></tr>'));
            $('#tr' + i).append($('<td style="padding: 3px">' + (i + 1) + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" hidden id="tdid' + i + '">' + res[i]['id'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdname' + i + '">' + res[i]['name'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdphone' + i + '">' + res[i]['phone'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdemail' + i + '">' + res[i]['email'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tddate' + i + '">' + res[i]['date'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdstreet' + i + '">' + res[i]['street'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdbuilding' + i + '">' + res[i]['building'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px"><textarea rows="7" cols="30" id="tdmessage' + i + '">' + res[i]['message']+"\n\r\n\rАвтор: "+ res[i]['name'] + '</textarea></td>'));
            $('#tr' + i).append($('<td style="padding: 3px"><input id="tdtitle' + i + '" type="text" placeholder="Заголовок"/></td>'));
            $('#tr' + i).append($('<td><button id="sendbutton' + (i) + '" onclick="sendRequestToForum(this)">' + 'Відправити на форум' + '</button></td>'));
            $('#tr' + i).append($('<td><button id="deletebutton' + (i) + '" onclick="deleteRequestFromDB(this)">' + 'Видалити' + '</button></td>'));
        }
    }
}

function getCheckedRequests(){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getCheckedRequests.php", false);
    request.send(null);

    var res = JSON.parse(request.responseText);
    $('#divtable').empty();
    if (res.length!=0) {
        var table = $('<table id="tableid" style="border-collapse: collapse"></table>');
        $('#divtable').append(table);
        $('#tableid').append($('<tr><th style="padding: 3px">№</th>' +
            '<th style="padding: 3px">Ім"я відправника</th>' +
            '<th style="padding: 3px">Телефон</th>' +
            '<th style="padding: 3px">Email</th>' +
            '<th style="padding: 3px">Дата</th>' +
            '<th style="padding: 3px">Вулиця</th>' +
            '<th style="padding: 3px">Будинок</th>' +
            '<th style="padding: 3px">Заголовок</th>'+
            '<th style="padding: 3px">Повідомлення</th></tr>'
            ));
        for (var i = 0; i < res.length; i++) {
            $('#tableid').append($('<tr id="tr' + i + '"></tr>'));
            $('#tr' + i).append($('<td style="padding: 3px">' + (i + 1) + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" hidden id="tdid' + i + '">' + res[i]['id'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdname' + i + '">' + res[i]['name'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdphone' + i + '">' + res[i]['phone'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdemail' + i + '">' + res[i]['email'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tddate' + i + '">' + res[i]['date'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdstreet' + i + '">' + res[i]['street'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdbuilding' + i + '">' + res[i]['building'] + '</td>'));
            $('#tr' + i).append($('<td style="padding: 3px" id="tdtitle' + i + '">'+res[i]['title']+'</td>'));
            $('#tr' + i).append($('<td style="padding: 3px"><textarea readonly rows="7" cols="50" id="tdmessage' + i + '">' + res[i]['message']+ '</textarea></td>'));
        }
    }
}

function sendRequestToForum(e){
    var i = e.id.replace('sendbutton','');
    var street = $('#tdstreet'+i).text();
    var building = $('#tdbuilding'+i).text();
    var message = $('#tdmessage'+i).val();
    var title = $('#tdtitle'+i).val();

    if (title==="" || message===""){
        $('#message').css({"color":"red"});
        $('#message').html("Поля \"Повідомлення\" та \"Заголовок\" повинні бути не порожніми!");
        return;
    }

    if (!check()){
        var mssg = $('#message').text();
        mssg = "Дані на форумі та в базі даних не співпадають!!!<br>"+mssg;
        $('#message').html(mssg);
        return;
    }

    var content = new Object();
    content.street = street;
    content.building = building;
    content.message = message;
    content.title = title;

    var request = new XMLHttpRequest();
    request.open("POST", "includes/requestToForum.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==='1'){
        $('#message').css({"color":"red"});
        $('#message').html("Помилка! Перевірте дані!");
    }
    else {
        $('#message').css({"color":"blue"});
        $('#message').html("Заявка була переміщена на форум.");
        saveToChecked(content, i);
        $('#deletebutton'+i).click();
    }
}

function saveToChecked(content, i){
    content.name = $('#tdname'+i).text();
    content.phone = $('#tdphone'+i).text();
    content.email = $('#tdemail'+i).text();
    content.date = $('#tddate'+i).text();

    var request = new XMLHttpRequest();
    request.open("POST","includes/saveToChecked.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==="SUCCESS"){
        getRequests();
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}

function deleteRequestFromDB(e){
    var i = e.id.replace('deletebutton','');
    var id = $('#tdid'+i).text();

    var content = new Object();
    content.id = id;

    var request = new XMLHttpRequest();
    request.open("POST","includes/deleteRequestFromDB.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==="SUCCESS"){
        location.reload();
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+request.responseText);
    }
}

function check(){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getBuildingsStreets.php", false);
    request.send(null);
    var res = JSON.parse(request.responseText);

    request.open("POST", "includes/getForumBuildingStreets.php", false);
    request.send(null);
    var res1 = JSON.parse(request.responseText);

    var mapDb = new Map();
    var i;
    for (i=0;i<res.length;i++){
        if (!mapDb.has(res[i]['street'])){
            mapDb.set(res[i]['street'],[]);
        }
        var buildingsArray = mapDb.get(res[i]['street']);
        buildingsArray.push(res[i]['building']);
    }

    var mapForum = new Map();
    for (i=0;i<res1.length;i++){
        if (!mapForum.has(res1[i][0])){
            mapForum.set(res1[i][0],[]);
        }
        var forumArray = mapForum.get(res1[i][0]);
        forumArray.push(res1[i][1]);
    }

    if (mapDb.size != mapForum.size){
        $('#message').css({"color":"red"});
        $('#message').html("Кількість вулиць в БД("+mapDb.size+") не співпадає з кількістю тем на форумі("+mapForum.size+").");
        return false;
    }
    var flag = false;
    for (var keyDb of mapDb.keys()) {
        for (var keyForum of mapForum.keys()){
            if (keyDb === keyForum) {
                var buildingsDb = mapDb.get(keyDb);
                var buildingsForum = mapForum.get(keyForum);
                if (buildingsDb.length != buildingsForum.length) {
                    $('#message').css({"color": "red"});
                    $('#message').html("Кількість будинків на вулиці \"" + keyDb + "\" в БД(" + buildingsDb.length + ") не співпадає з кількістю тем на форумі(" + buildingsForum.length + ").");
                    return false;
                }
                buildingsDb.sort();
                buildingsForum.sort();
                for (i = 0; i < buildingsDb.length; i++) {
                    if (buildingsDb[i] != buildingsForum[i]) {
                        $('#message').css({"color": "red"});
                        $('#message').html("Для будинку в БД \"" + buildingsDb[i] + "\" немає відповідної теми на форумі.");
                        return false;
                    }
                }
                break;
            }
        }
    };
    $('#message').css({"color":"blue"});
    $('#message').html("Будинки і вулиці в БД співпадають з темами на форумі.");
    return true;
}

function checkAccordance(){
    $('#page-wrapper').empty();
    $('#page-wrapper').append($('<p id="message"></p>'));
    check();
}

function loadLastRequests(){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getThreeLastRequests.php", false);
    request.send(null);
    var res = JSON.parse(request.responseText);
    if (res.length==0){
        return;
    }
    $('.fa-envelope-o').addClass('fa-envelope').removeClass('fa-envelope-o');
    var li = $('#messagesList');
    var ul = $('<ul class="dropdown-menu dropdown-messages"></ul>');
    for (var i=0;i<res.length;i++){
        var title = $('<strong>'+res[i]['street']+', '+res[i]['building']+'</strong>');
        var date = $('<span class="pull-right text-muted"><em>'+res[i]['date']+'</em></span>');
        var text = $('<div>'+res[i]['message']+'<br><i>Автор: '+res[i]['name']+'</i></div>');
        var head = $('<div></div>');
        var link = $('<a></a>');
        var li1 = $('<li></li> ');
        head.append(title);
        head.append(date);
        link.append(head);
        link.append(text);
        li1.append(link);
        ul.append(li1);
        ul.append($('<li class="divider"></li>'));
    }
    ul.append($('<li><a class="text-center" href="#" onclick="requests()"><strong>Завантажити всі заявки </strong><i class="fa fa-angle-right"></i></a></li>'));
    li.append(ul);
}