function addNews(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Додати новину</h1>'));
    wrapper.append($('<label>Тип:</label><br><select id="type"><option value="done_news">Виконані</option>' +
        '<option value="planned_news">Заплановані</option><option value="actual_news">Актуальні</option></select><br><label>Заголовок:</label><br>' +
        '<input type="text" id="title" required data-validation-required-message="Заголовок"/><br>' +
        '<label id="label_picture">Картинка:</label><br><select id="picture" onchange="onChange()"><option value="" id="delete"></option></select>' +
        '<div id="imgplace"></div><br><label>Текст новини:</label><br><textarea rows="9" cols="140" id="textarea" ' +
        'required data-validation-required-message="Текст новости" maxlength="10000" style="resize:none"></textarea><br>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<button id="button" onclick="saveNewsToDB()">Додати новину</button>'));
    new nicEditor().panelInstance('textarea');
    getImages('newsIcons');
}

var typeNews;

function onChange(){
    $('#delete').remove();
    $('#imgplace').html('<img src="'+$('#picture option:selected').text()+'" width="100px"/>');
}


function getImages(type){
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getImages.php", false);
    request.setRequestHeader("Content-type", "application/json");
    var content = new Object();
    content.type = type;
    request.send(JSON.stringify(content));
    if (request.responseText.length===0){
        $('#message').css({"color":"red"});
        $('#message').html('Таблиця \"images\" порожня!');
        return;
    }
    var res = JSON.parse(request.responseText);
    for (var i=0;i<res.length;i++){
        $('#picture').append($('<option value="'+res[i]['LINK']+'">'+'/'+res[i]['LINK'].substring(res[i]['LINK'].indexOf('images'))+'</option>'))
    }
}

function saveNewsToDB(){
    var type = $('#type option:selected').val();
    var title = $('#title').val();
    var image =  $('#picture option:selected').val();
    //var text = $('#textarea').val();
    var text = $('.nicEdit-main').html();
    var content = new Object();
    content.type = type;
    content.title = title;
    content.image = image;
    content.text = text;
    content.date = new Date();
    content.author = "";

    var req = new XMLHttpRequest();
    req.open("POST", "includes/saveNewsToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));
    if (req.responseText==="SUCCESS"){
        $('#message').css({"color":"blue"});
        $('#message').html('Новина добавлена!');
        $('#title').val('');
        $('#textarea').val('');
        $('#imgplace').val('');
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}

function editNews(){
    var wrapper = $('#page-wrapper');
    wrapper.empty();
    wrapper.append($('<h1>Редагувати новину</h1>'));
    wrapper.append($('<p id="message"></p>'));
    wrapper.append($('<button onclick="getNews(\'planned\')">Завантажити заплановані роботи</button>' +
    '<button onclick="getNews(\'done\')">Завантажити виконані роботи</button><button onclick="getNews(\'actual\')">Завантажити актуальні новини</button>'));
    var div = $('<div id="divtable"></div>');
    wrapper.append(div);
}

function getNews(type) {
    var content = new Object();
    content.type = type;
    typeNews = type;
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getNews.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    var res = JSON.parse(request.responseText);

    if (res.length == 0) {
        $('#message').css({"color": "red"});
        $('#message').html('Таблиця \"' + type + '_news\" порожня');
        $('#divtable').empty();
        return;

    }
    var table = $('<table id="tableid"></table>');
    $('#divtable').empty();
    $('#divtable').append(table);
    $('#tableid').append($('<tr><th>№</th><th>Заголовок</th><th>Картинка</th><th>Текст</th><th>Дата</th></tr>'));
    for (var i = 0; i < res.length; i++) {
        $('#tableid').append($('<tr id="tr' + i + '"></tr>'));
        $('#tr' + i).append($('<td>' + (i + 1) + '</td>'));
        $('#tr' + i).append($('<td id="tdid' + i + '" hidden>' + res[i]['id'] + '</td>'));
        $('#tr' + i).append($('<td id="tdtitle' + i + '">' + res[i]['title'] + '</td>'));
        $('#tr' + i).append($('<td id="tdimage' + i + '"><img id="img' + i + '" src="' + '/' + res[i]['image'].substring(res[i]['image'].indexOf('images')) + '" width="50px"/></td>'));
        $('#tr' + i).append($('<td><textarea readonly rows="5" cols="20" style="resize: none" id="tdtext' + i + '">' + res[i]['text'] + '</textarea></td>'));
        $('#tr' + i).append($('<td id="tddate' + i + '">' + res[i]['date'] + '</td>'));
        $('#tr' + i).append($('<td><button id="editbutton' + (i) + '" onclick="edit(this)">' + 'Редагувати' + '</button></td>'));
        $('#tr' + i).append($('<td><button id="deletebutton' + (i) + '" onclick="newsToArchive(this)">' + 'Перенести в архів(видалити)' + '</button></td>'));
    }
}

function newsToArchive(e){
    var i = e.id.replace('deletebutton','');
    var id = $('#tdid'+i).text();

    var content = new Object();
    content.type = typeNews;
    content.id = id;

    var request = new XMLHttpRequest();
    request.open("POST", "includes/saveNewsToArchiveDB.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==="SUCCESS"){
        if(typeNews=="actual"){
            alert("Новина з id=\'" + id + "\' була видалена.");
            getNews(typeNews);
        }
        else {
            alert("Новина з id=\'" + id + "\' була перенесена в архів.");
            getNews(typeNews);
        }
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}

function edit(e){
    var i = e.id.replace('editbutton','');
    var id = $('<input hidden type="text" id="id" value="'+$('#tdid'+i).text()+'"/>');
    var title = $('<label>Заголовок:</label><br><input type="text" id="title" value="'+$('#tdtitle'+i).text()+'"/><br>');
    var request = new XMLHttpRequest();
    request.open("POST", "includes/getImages.php", false);
    request.send(null);
    if (request.responseText.length===0){
        $('#message').css({"color":"red"});
        $('#message').html('Таблиця \"images\" порожня!');
        return;
    }
    var picture = $('<label>Картинка:</label><br>');
    var select = $('<select id="picture" onchange="onChange()"></select><br>');
    var res = JSON.parse(request.responseText);
    for(var j=0;j<res.length;j++){
        if ('/'+res[j]['LINK'].substring(res[j]['LINK'].indexOf('images'))===$('#img'+i).attr("src")){
            select.append('<option selected value="'+res[j]['LINK']+'">'+'/'+res[j]['LINK'].substring(res[i]['LINK'].indexOf('images'))+'</option>');
        }
        else{
            select.append('<option value="'+res[j]['LINK']+'">'+'/'+res[j]['LINK'].substring(res[i]['LINK'].indexOf('images'))+'</option>');
        }
    }
    var imgplace = $('<div id="imgplace"></div>');

    var text = $('<label>Текст новини:</label><br><textarea rows="10" cols="145" id="textarea" required data-validation-' +
        'required-message="Текст новини" maxlength="10000" style="resize:none">'+$('#tdtext'+i).text()+'</textarea><br>');
    var button = $('<button onclick="updateNewsToDB()">Редагувати новину</button>')
    var wrapper = $('#divtable');
    wrapper.empty();
    wrapper.append(id);
    wrapper.append(title);
    wrapper.append(picture);
    wrapper.append(select);
    wrapper.append(imgplace);
    wrapper.append(text);
    wrapper.append($('<p id="message"></p>'));
    wrapper.append(button);
    new nicEditor().panelInstance('textarea');
}
function edit_actual_news(e){
    var i = e.id.replace('editbutton','');
    var id = $('<input hidden type="text" id="id" value="'+$('#tdid'+i).text()+'"/>');
    var title = $('<label>Заголовок:</label><br><input type="text" id="title" value="'+$('#tdtitle'+i).text()+'"/><br>');
    var text = $('<label>Текст новини:</label><br><textarea rows="10" cols="145" id="textarea" required data-validation-' +
        'required-message="Текст новини" maxlength="10000" style="resize:none">'+$('#tdtext'+i).text()+'</textarea><br>');
    var button = $('<button onclick="updateNewsToDB()">Редагувати новину</button>')
    var wrapper = $('#divtable');
    wrapper.empty();
    wrapper.append(id);
    wrapper.append(title);
    wrapper.append(text);
    wrapper.append($('<p id="message"></p>'));
    wrapper.append(button);
}
function updateNewsToDB(){
    var id = $('#id').val();
    var title = $('#title').val();
    //var text = $('#textarea').val();
    var text = $('.nicEdit-main').html();
    var content = new Object();
    content.type = typeNews;
    content.id = id;
    content.title = title;
    content.text = text;
    content.date = new Date();
    content.image = $('#picture option:selected').val();
    if(typeNews!="actual") {
        content.author = "";
    }

    var req = new XMLHttpRequest();
    req.open("POST", "includes/updateNewsToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));

    if (req.responseText==="SUCCESS"){
        alert('Новина була оновлена.');
        getNews(typeNews);
    }
    else{
        $('#message').css({"color":"red"});
        $('#message').html("Помилка БД!<br>"+req.responseText);
    }
}