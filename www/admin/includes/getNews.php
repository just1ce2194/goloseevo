<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if (strcasecmp($jsonArray['type'],"planned")==0) {
    $query = sprintf("SELECT n.id, n.title, i.link, n.news_text, n.news_date, author FROM planned_news n, images i
                  WHERE n.image=i.image_id");
    $result = mysqli_query($db, $query) or die('Query failed: ' . mysql_error());
    $db->close();
    $res = array();
    while ($row = mysqli_fetch_array($result)) {
        $arr['id'] = $row[0];
        $arr['title'] = $row[1];
        $arr['image'] = $row[2];
        $arr['text'] = $row[3];
        $arr['date'] = $row[4];
        $arr['author'] = $row[5];
        array_push($res, $arr);
    }
    echo json_encode($res);
}
else if (strcasecmp($jsonArray['type'],"done")==0) {
    $query = sprintf("SELECT n.id, n.title, i.link, n.news_text, n.news_date, author FROM done_news n, images i
                  WHERE n.image=i.image_id");
    $result = mysqli_query($db, $query) or die('Query failed: ' . mysql_error());
    $db->close();
    $res = array();
    while ($row = mysqli_fetch_array($result)) {
        $arr['id'] = $row[0];
        $arr['title'] = $row[1];
        $arr['image'] = $row[2];
        $arr['text'] = $row[3];
        $arr['date'] = $row[4];
        $arr['author'] = $row[5];
        array_push($res, $arr);
    }
    echo json_encode($res);
}
else if (strcasecmp($jsonArray['type'],"actual")==0) {
    $query = sprintf("SELECT n.id, n.title, i.link, n.news_text, n.news_date FROM actual_news n, images i
                  WHERE n.image=i.image_id");
    $result = mysqli_query($db, $query) or die('Query failed: ' . mysql_error());
    $db->close();
    $res = array();
    while ($row = mysqli_fetch_array($result)) {
        $arr['id'] = $row[0];
        $arr['title'] = $row[1];
        $arr['image']= $row[2];
        $arr['text'] = $row[3];
        $arr['date'] = $row[4];

        array_push($res, $arr);
    }
    echo json_encode($res);
}
