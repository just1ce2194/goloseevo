<?php
header('Content-Type: text/html; charset=utf-8');
include_once(dirname(dirname(dirname(__FILE__)))."/php/"."psl-config.php");
if (unlink($_POST['image'])){
    echo '<script>alert("Картинка видалена з сервера.");</script>';
    deleteImageFromDb($_POST['image']);
}
else{
    echo '<script>alert("Помилка видалення файлу.");location.href=\'../\'</script>';

}
function deleteImageFromDb($path){
    include('db.php');
    if ($stmt = $db->prepare('DELETE FROM images WHERE link=?')) {
        $stmt->bind_param('s',$path);
        if (!$stmt->execute()){
            echo '<script>alert("'.$stmt->error.'");location.href=\'../\'</script>';
        }
        $stmt->close();
    }
    echo '<script>alert("Картинка видалена з бази даних.");location.href=\'../\'</script>';
    $db->close();
}
?>