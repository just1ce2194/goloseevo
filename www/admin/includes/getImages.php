<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if (strcasecmp($jsonArray['type'],'')==0) {
    $result = mysqli_query($db, "SELECT link FROM images");
    $db->close();
    $return_arr = array();
    while ($row = mysqli_fetch_array($result)) {
        $row_array['LINK'] = $row[0];
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
}
else{
    if ($stmt = $db->prepare('SELECT link
                              FROM images i, image_types it
                              WHERE it.image_type=? AND i.image_type=it.type_id')){

        $stmt->bind_param('s', $jsonArray['type']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->bind_result($link);
        $res = array();
        while ($stmt->fetch()) {
            $r['LINK'] = $link;
            array_push($res, $r);
        }
        $stmt->close();
    }
    $db->close();
    echo json_encode($res);
}