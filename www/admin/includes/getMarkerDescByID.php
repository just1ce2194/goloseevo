<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('SELECT description FROM markers WHERE id=?')){

    $stmt->bind_param('s', $jsonArray['id']);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->bind_result($id);
    $res = array();
    while ($stmt->fetch()) {
        $r['DESC'] = $id;
        array_push($res, $r);
    }
    $stmt->close();
}
$db->close();
echo json_encode($res);
