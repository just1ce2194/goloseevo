<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('INSERT INTO markers (description, lat,lng , imageid) VALUES (?,?,?, (SELECT image_id FROM images i
    WHERE i.link=?))')) {
    $stmt->bind_param('ssss',$jsonArray['desc'], $jsonArray['lat'], $jsonArray['lng'], $jsonArray['imagelink']);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
echo "SUCCESS";
$db->close();
