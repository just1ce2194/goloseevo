<?php
header('Content-Type: text/html; charset=utf-8');
include_once(dirname(dirname(dirname(__FILE__)))."/php/"."psl-config.php");
if($_FILES['filename']['size'] > 1024*MAX_SIZE_IMAGE*1024)
{
    echo '<script>alert("Помилка!\\nРозмір файлу перевищує 5MB.");location.href=\'../\'</script>';
    exit;
}
if($_FILES['filename']['type'] != 'image/png' && $_FILES['filename']['type'] != 'image/jpeg'  && $_FILES['filename']['type'] != 'image/pjpeg'){
    echo ('<script>alert("Помилка!\\nТип файлу повинен бути \'*.png\' або \'*.jpeg\'.");location.href=\' ../\'</script>');
    exit;
}
if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
{
    $path = ROOT."images/".$_POST["imageType"]."/".$_FILES["filename"]["name"];
    move_uploaded_file($_FILES["filename"]["tmp_name"], iconv("utf-8", "cp1251", $path));
    echo '<script>alert("Картинка завантажена на сервер.");</script>';
    saveToDB($path, $_POST["imageType"]);
} else {
    echo '<script>alert("Помилка завантаження файлу.");location.href=\'../\'</script>';
}

function saveToDB($path, $imageType){
    include('db.php');
    if ($stmt = $db->prepare('INSERT INTO images (link, image_type) VALUES (?,
                              (SELECT TYPE_ID FROM image_types WHERE image_type=?)
                              )')) {
        $stmt->bind_param('ss',$path, $imageType);
        if (!$stmt->execute()){
            echo '<script>alert("'.$stmt->error.'");location.href=\'../\'</script>';
        }
        $stmt->close();
    }
    echo '<script>alert("Картинка додана в базу даних.");location.href=\'../\'</script>';
    $db->close();
}
?>