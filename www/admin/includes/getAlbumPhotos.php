<?php
header('Content-Type: text/html; charset=utf-8');
include("db.php");
$str_json = file_get_contents('php://input');
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('SELECT link FROM photos WHERE album=(SELECT album_id FROM albums WHERE name=?)')) {
    $stmt->bind_param('s', $jsonArray);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->bind_result($link);
    $res = array();
    while ($stmt->fetch()) {
        $r = array();
        $r['link'] = $link;
        array_push($res, $r);
    }
    $stmt->close();
}
$db->close();
echo json_encode($res);