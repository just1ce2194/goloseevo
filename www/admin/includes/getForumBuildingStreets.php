<?php
include("dbForum.php");

if ($stmt = $db->prepare('SELECT st_name, name
                          FROM forums
                          LEFT JOIN (
                            SELECT id as st_id, name as st_name
                            FROM forums
                            WHERE parent_id=(
                              SELECT id FROM forums WHERE name=?
                            )
                          ) as str
                          ON str.st_id=forums.parent_id    ')) {
    $mf = MAIN_FORUM;
    $stmt->bind_param('s', $mf);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->bind_result($name, $id);
    $res = array();
    while ($stmt->fetch()) {
        $r = array();
        if ($name!="") {
            array_push($r, $name);
            array_push($r, $id);
            array_push($res, $r);
        }

    }
    $stmt->close();
}
$db->close();
echo json_encode($res);