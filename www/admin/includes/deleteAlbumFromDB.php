<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('DELETE FROM photos WHERE album=(SELECT album_id FROM albums WHERE name=?)')) {
    $stmt->bind_param('s',$jsonArray);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
if ($stmt = $db->prepare('SELECT link FROM albums WHERE name=?')) {
    $stmt->bind_param('s', $jsonArray);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->bind_result($link);

    while ($stmt->fetch()) {
        $res = $link;
    }
    $stmt->close();
}
if ($stmt = $db->prepare('DELETE FROM albums WHERE name=?')) {
    $stmt->bind_param('s',$jsonArray);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
function rrmdir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir."/".$object))
                    rrmdir($dir."/".$object);
                else
                    unlink($dir."/".$object);
            }
        }
        rmdir($dir);
    }
}

rrmdir($res);
echo "SUCCESS";
$db->close();