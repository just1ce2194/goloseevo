<?php
include("db.php");
$result = mysqli_query($db,"SELECT image_type FROM image_types");
$db->close();
$return_arr = array();
while ($row = mysqli_fetch_array($result)) {
    $row_array['type'] = $row[0];
    array_push($return_arr,$row_array);
}
echo json_encode($return_arr);