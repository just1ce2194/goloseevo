<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('DELETE FROM markers WHERE id=?')) {
    $stmt->bind_param('d',$jsonArray['id']);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
echo "SUCCESS";
$db->close();