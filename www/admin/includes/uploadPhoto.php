<?php
header('Content-Type: text/html; charset=utf-8');
include_once(dirname(dirname(dirname(__FILE__)))."/php/"."psl-config.php");
if (strcasecmp($_POST["imageType"],"")==0){
    echo ('<script>alert("Ви не вибрали альбом!");location.href=\' ../\'</script>');
    exit;
}
if($_FILES['filename']['size'] > 1024*MAX_SIZE_IMAGE*1024)
{
    echo '<script>alert("Помилка!\\nРозмір файлу перевищує 5MB.\\nSize is " + $_FILES[\'filename\'][\'size\']);location.href=\'../\'</script>';
    exit;
}
if($_FILES['filename']['type'] != 'image/png' && $_FILES['filename']['type'] != 'image/jpeg'  && $_FILES['filename']['type'] != 'image/pjpeg'){
    echo ('<script>alert("Помилка!\\nТип файлу повинен бути \'*.png\' або \'*.jpeg\'.");location.href=\' ../\'</script>');
    exit;
}
if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
{
    include('db.php');

    if ($stmt = $db->prepare('SELECT link FROM albums WHERE name=?')) {
        $stmt->bind_param('s', $_POST["imageType"]);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->bind_result($link);
        while ($stmt->fetch()) {
           $res=$link;
        }
        $stmt->close();
    }

    $path = $res."/".$_FILES["filename"]["name"];
    move_uploaded_file($_FILES["filename"]["tmp_name"], iconv("utf-8", "cp1251", $path));
    echo '<script>alert("Фото завантажене на сервер.");</script>';
    saveToDB($path, $_POST["imageType"], $_POST["description"]);
} else {
    echo '<script>alert("Помилка завантаження файлу.");location.href=\'../\'</script>';
}

function saveToDB($path, $imageType, $description){
    include('db.php');
    if ($stmt = $db->prepare('INSERT INTO photos (link, description, album) VALUES (?,?,
                              (SELECT album_id FROM albums WHERE name=?)
                              )')) {
        $stmt->bind_param('sss',$path, $description, $imageType);
        if (!$stmt->execute()){
            echo '<script>alert("'.$stmt->error.'");location.href=\'../\'</script>';
        }
        $stmt->close();
    }
    echo '<script>alert("Фото додане в базу даних.");location.href=\'../\'</script>';
    $db->close();
}
?>