<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('INSERT INTO buildings (number, center, coordinates, picture, info, link_to_forum, street) VALUES (?,?,?,?,?,?,
    (SELECT street_id FROM streets WHERE name=?))')) {
    $stmt->bind_param('sssssss',$jsonArray['number'], $jsonArray['center'], $jsonArray['coordinates'], $jsonArray['picture'], $jsonArray['info'],
        $jsonArray['linktoforum'], $jsonArray['street']);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
echo "SUCCESS";
$db->close();