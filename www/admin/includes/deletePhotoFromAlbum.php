<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('DELETE FROM photos WHERE link=?')) {
    $stmt->bind_param('s',$jsonArray);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
echo "SUCCESS";
unlink(iconv("utf-8", "cp1251", $jsonArray));
$db->close();