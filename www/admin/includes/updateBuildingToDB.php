<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('UPDATE buildings
                          SET center=?, coordinates=?, picture=?, info=?, link_to_forum=?
                          WHERE building_id=?')) {
    $stmt->bind_param('sssssd',$jsonArray['center'], $jsonArray['coordinates'], $jsonArray['picture'],
        $jsonArray['info'], $jsonArray['linktoforum'],$jsonArray['id']);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
echo "SUCCESS";
$db->close();
