<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if (strcasecmp($jsonArray['type'],"planned")==0) {
    if ($stmt = $db->prepare('INSERT INTO planned_news_archive (title, image, news_text, news_date, author) VALUES (
      (SELECT title FROM planned_news WHERE id=?),
      (SELECT image FROM planned_news WHERE id=?),
      (SELECT news_text FROM planned_news WHERE id=?),
      (SELECT news_date FROM planned_news WHERE id=?),
      (SELECT author FROM planned_news WHERE id=?)
    )')) {
        $stmt->bind_param('ddddd', $jsonArray['id'],$jsonArray['id'],$jsonArray['id'],$jsonArray['id'],$jsonArray['id']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->close();
    }
    if ($stmt = $db->prepare('DELETE FROM planned_news WHERE id=?')) {
        $stmt->bind_param('d', $jsonArray['id']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->close();
    }
    echo "SUCCESS";
    $db->close();
}
else if (strcasecmp($jsonArray['type'],"done")==0) {
    if ($stmt = $db->prepare('INSERT INTO done_news_archive (title, image, news_text, news_date, author) VALUES (
      (SELECT title FROM done_news WHERE id=?),
      (SELECT image FROM done_news WHERE id=?),
      (SELECT news_text FROM done_news WHERE id=?),
      (SELECT news_date FROM done_news WHERE id=?),
      (SELECT author FROM done_news WHERE id=?)
    )')) {
        $stmt->bind_param('ddddd', $jsonArray['id'],$jsonArray['id'],$jsonArray['id'],$jsonArray['id'],$jsonArray['id']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->close();
    }
    if ($stmt = $db->prepare('DELETE FROM done_news WHERE id=?')) {
        $stmt->bind_param('d', $jsonArray['id']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->close();
    }
    echo "SUCCESS";
    $db->close();
}
else if (strcasecmp($jsonArray['type'],"actual")==0) {
    if ($stmt = $db->prepare('DELETE FROM actual_news WHERE id=?')) {
        $stmt->bind_param('d', $jsonArray['id']);
        if (!$stmt->execute()){
            echo $stmt->error;
        }
        $stmt->close();
    }
    echo "SUCCESS";
    $db->close();
}