<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
$name = count(scandir('../../images/albums/'))+1;
while (file_exists('../../images/albums/'.$name)){
    $name=$name+1;
}
mkdir('../../images/albums/'.$name, 0777, true);
$name = str_replace('\\','/',dirname(dirname(dirname(__FILE__)))).'/images/albums/'.$name;
if ($stmt = $db->prepare('INSERT INTO albums (name, link) VALUES (?,?)')) {
    $stmt->bind_param('ss',$jsonArray, $name );
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
echo "SUCCESS";

$db->close();