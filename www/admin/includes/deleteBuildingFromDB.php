<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('DELETE FROM buildings WHERE building_id=?')) {
    $stmt->bind_param('s',$jsonArray);
    if (!$stmt->execute()){
        echo $stmt->error;
    }
    $stmt->close();
}
echo "SUCCESS";
$db->close();