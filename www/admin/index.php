<?php
include_once 'includes/db.php';
include_once 'includes/functions.php';

sec_session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Панель Адміністратора</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
    <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
</head>

<body onload="loadLastRequests()">
<?php if (login_check($db) == true) : ?>
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Ласкаво просимо, <?php echo htmlentities($_SESSION['username']); ?>!</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown" id="messagesList">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope-o fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
<!--
                    <!-- /.dropdown-messages -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Профіль</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Налаштування</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="includes/logout.php"><i class="fa fa-sign-out fa-fw"></i> Вихід</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#">Новини<span class="fa arrow "></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" onclick="addNews()">Додати новину</a>
                                </li>
                                <li>
                                    <a href="#" onclick="editNews()">Редагувати новину</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Заявки<span class="fa arrow "></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" onclick="requests()">Необроблені заявки</a>
                                </li>
                                <li>
                                    <a href="#" onclick="checkedRequests()">Оброблені заявки</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Фотографії<span class="fa arrow "></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" onclick="addAlbum()">Додати альбом</a>
                                </li>
                                <li>
                                    <a href="#" onclick="addPhotoToAlbum()">Додати фото в альбом</a>
                                </li>
                                <li>
                                    <a href="#" onclick="deletePhotoFromAlbum()">Видалити фото з альбому</a>
                                </li>
                                <li>
                                    <a href="#" onclick="deleteAlbum()">Видалити альбом</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Картинки<span class="fa arrow "></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" onclick="uploadImage()">Завантажити картинку</a>
                                </li>
                                <li>
                                    <a href="#" onclick="deleteImage()">Видалити картинку</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Маркери<span class="fa arrow "></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" onclick="addMarker()">Додати маркер на карту</a>
                                </li>
                                <li>
                                    <a href="#" onclick="deleteMarker()">Видалити маркер</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Будинки<span class="fa arrow "></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" onclick="addStreet()">Додати вулицю</a>
                                </li>
                                <li>
                                    <a href="#" onclick="loadStreets()">Редагувати вулицю</a>
                                </li>
                                <li>
                                    <a href="#" onclick="addPolygon()">Додати будинок(полігон)</a>
                                </li>
                                <li>
                                    <a href="#" onclick="editPolygons()">Редагувати будинок(полігон)</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onclick="checkAccordance()">Перевірити відповідність</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php else :
    echo '<script>location.href="login.php"</script>';
 endif; ?>

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

    <script src="js/custom/streets.js"></script>
    <script src="js/custom/polygons.js"></script>
    <script src="js/custom/news.js"></script>
    <script src="js/custom/requests.js"></script>
    <script src="js/custom/images.js"></script>
    <script src="js/custom/markers.js"></script>
    <script src="js/custom/photos.js"></script>
    <script src="bower_components/metisMenu/src/metisMenu.js"></script>
</body>

</html>
