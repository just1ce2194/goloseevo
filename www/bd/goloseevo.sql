-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Янв 25 2016 г., 18:24
-- Версия сервера: 10.0.17-MariaDB
-- Версия PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `knztrquq_goloseevo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `actual_news`
--

CREATE TABLE IF NOT EXISTS `actual_news` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` text NOT NULL,
  `NEWS_TEXT` text NOT NULL,
  `NEWS_DATE` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `album_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `link` varchar(200) NOT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Дамп данных таблицы `albums`
--

INSERT INTO `albums` (`album_id`, `name`, `link`) VALUES
(30, 'Особисті фото', '/home/knztrquq/public_html/images/albums/3'),
(31, 'За роботою', '/home/knztrquq/public_html/images/albums/4');

-- --------------------------------------------------------

--
-- Структура таблицы `buildings`
--

CREATE TABLE IF NOT EXISTS `buildings` (
  `BUILDING_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NUMBER` varchar(10) NOT NULL,
  `CENTER` varchar(25) NOT NULL,
  `COORDINATES` varchar(500) NOT NULL,
  `PICTURE` varchar(500) NOT NULL,
  `INFO` varchar(1000) NOT NULL,
  `LINK_TO_FORUM` varchar(200) NOT NULL,
  `STREET` int(10) unsigned NOT NULL,
  PRIMARY KEY (`BUILDING_ID`),
  KEY `STREET_8` (`STREET`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=239 ;

--
-- Дамп данных таблицы `buildings`
--

INSERT INTO `buildings` (`BUILDING_ID`, `NUMBER`, `CENTER`, `COORDINATES`, `PICTURE`, `INFO`, `LINK_TO_FORUM`, `STREET`) VALUES
(8, '19/11', '50.391483, 30.493878', '50.391674, 30.494752; 50.391585, 30.494794; 50.391264, 30.49288; 50.391347, 30.492797; ', 'images/lomonosova19-11.jpg', 'Будинок №19/11. <br> Під''їздів - 2, квартир - 54 <br> мешканців -185', 'http://kalinichenko.com.ua/forum/index.php?/forum/7-1911/', 1),
(128, '12', '50.390968, 30.49344', '50.391223, 30.49373; 50.391152, 30.493848; 50.390699, 30.493124; 50.390771, 30.493011; ', 'images/buildings/Kolovyiskii_12.JPG', 'Будинок №12. <br> Під''їздів - 1, квартир - 27 <br> мешканців -103', 'http://kalinichenko.com.ua/forum/index.php?/forum/10-12/', 2),
(138, '4', '50.389089, 30.491815', '50.389043, 30.491311; 50.389142, 30.491303; 50.389143, 30.492335; 50.389043, 30.492335; ', '', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/18-4/', 3),
(139, '10К1', '50.388831, 30.488213', '50.388864, 30.487685; 50.388958, 30.487723; 50.388797, 30.488732; 50.388703, 30.488692; ', 'images/buildings/Sechenova_10k1.JPG', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/9-10%D0%BA1/', 3),
(141, '7', '50.389931, 30.492520', '50.389914, 30.492003; 50.390004, 30.492016; 50.389948, 30.493022; 50.389854, 30.493011; ', 'images/buildings/Kolovyiskii_7.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/26-7/', 2),
(142, '9', '50.390305, 30.492337', '50.390303, 30.491837; 50.390393, 30.491846; 50.390326, 30.492841; 50.390244, 30.492829; ', 'images/buildings/Kolovyiskii_9.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/28-9/', 2),
(143, '11', '50.390707, 30.492175', '50.390694, 30.491658; 50.390780, 30.491672; 50.390718, 30.492660; 50.390632, 30.492646; ', 'images/buildings/Kolovyiskii_11.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/30-11/', 2),
(144, '5/2', '50.389520, 30.492728', '50.389500, 30.492217; 50.389595, 30.492224; 50.389533, 30.493241; 50.389435, 30.493232; ', 'images/buildings/Kolovyiskii_5-2.JPG', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/25-52/', 2),
(145, '8', '50.390209, 30.493814', '50.390025, 30.493404; 50.390459, 30.494137; 50.390393, 30.494236; 50.389957, 30.493499; ', 'images/buildings/Kolovyiskii_8.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/27-8/', 2),
(146, '10', '50.390571, 30.493598', '50.390389, 30.493171; 50.390839, 30.493909; 50.390770, 30.494017; 50.390310, 30.493278; ', 'images/buildings/Kolovyiskii_10.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/29-10/', 2),
(147, '10К2', '50.389329, 30.488420', '50.389460, 30.487927; 50.389294, 30.488914; 50.389195, 30.488874; 50.389359, 30.487881; ', 'images/buildings/Sechenova_10k2.JPG', 'Під''їздів - 4<br>Квартир - 68', 'http://kalinichenko.com.ua/forum/index.php?/forum/21-10%D0%BA2/', 3),
(148, '10К3', '50.389772, 30.488426', '50.389734, 30.487897; 50.389903, 30.488903; 50.389809, 30.488943; 50.389642, 30.487935; ', 'images/buildings/Sechenova_10k3.JPG', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/22-10%D0%BA3/', 3),
(149, '31', '50.390516, 30.488138', '50.390394, 30.487653; 50.390494, 30.487615; 50.390647, 30.488607; 50.390549, 30.488645; ', 'images/buildings/Lomonosova_31.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/23-31/', 1),
(150, '31/2', '50.390138, 30.488278', '50.390114, 30.487747; 50.390281, 30.488753; 50.390177, 30.488794; 50.390010, 30.487793; ', 'images/buildings/Lomonosova_31-2.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/24-312/', 1),
(151, '29', '50.390657, 30.489378', '50.390631, 30.488787; 50.390792, 30.489910; 50.390694, 30.489955; 50.390525, 30.488850; ', 'images/buildings/Lomonosova_29.jpg', 'Під''їздів - 3<br>Квартир - 104', 'http://kalinichenko.com.ua/forum/index.php?/forum/31-29/', 1),
(152, '7', '50.387847, 30.490168', '50.387723, 30.489726; 50.388056, 30.490539; 50.387966, 30.490626; 50.387639, 30.489800; ', 'images/buildings/Sechenova_7.JPG', 'Під''їздів - 3<br>Квартир - 59', 'http://kalinichenko.com.ua/forum/index.php?/forum/32-7/', 3),
(154, '27', '50.390987, 30.491359', '50.390949, 30.490747; 50.391065, 30.491528; 50.391118, 30.491505; 50.391180, 30.491911; 50.391070, 30.491951; 50.391011, 30.491576; 50.390954, 30.491593; 50.390833, 30.490787; ', 'images/buildings/Lomonosova_27.jpg', 'Під''їздів - 3<br>Квартир - 103', 'http://kalinichenko.com.ua/forum/index.php?/forum/33-27/', 1),
(155, '13/23', '50.391176, 30.492403', '50.391340, 30.492240; 50.391372, 30.492396; 50.390973, 30.492566; 50.390951, 30.492403; ', 'images/buildings/Kolovyiskii_13-23.jpg', 'Під''їздів - 2<br>Квартир - 72', 'http://kalinichenko.com.ua/forum/index.php?/forum/34-1323/', 2),
(157, '24', '50.391460, 30.490154', '50.391399, 30.489386; 50.391632, 30.490866; 50.391519, 30.490923; 50.391282, 30.489437; ', 'images/buildings/Lomonosova_24.jpg', 'Під''їздів - 4<br>Квартир - 143', 'http://kalinichenko.com.ua/forum/index.php?/forum/36-24/', 1),
(158, '22/15', '50.391659, 30.491800', '50.391621, 30.491240; 50.391788, 30.492267; 50.391680, 30.492306; 50.391512, 30.491280; ', 'images/buildings/Lomonosova_22-15.jpg', 'Під''їздів - 4<br>Квартир - 58', 'http://kalinichenko.com.ua/forum/index.php?/forum/35-2215/', 1),
(159, '26', '50.391168, 30.488785', '50.391156, 30.488341; 50.391287, 30.489172; 50.391175, 30.489214; 50.391049, 30.488385; ', 'images/buildings/Lomonosova_26.JPG', 'Під''їздів - 4<br>Квартир - 40', 'http://kalinichenko.com.ua/forum/index.php?/forum/37-26/', 1),
(160, '18К1', '50.392586, 30.492414', '50.392815, 30.492225; 50.392839, 30.492395; 50.392377, 30.492578; 50.392347, 30.492408; ', 'images/buildings/Kolovyiskii_18k1.jpg', 'Під''їздів - 3<br>Квартир - 48', 'http://kalinichenko.com.ua/forum/index.php?/forum/38-18%D0%BA1/', 2),
(161, '18К2', '50.392737, 30.493117', '50.392877, 30.492965; 50.392904, 30.493157; 50.392839, 30.493185; 50.392833, 30.493152; 50.392666, 30.493219; 50.392667, 30.493259; 50.392597, 30.493279; 50.392570, 30.493084; ', 'images/buildings/Kolovyiskii_18k2.jpg', 'Під''їздів - 2<br>Квартир - 14', 'http://kalinichenko.com.ua/forum/index.php?/forum/39-18%D0%BA2/', 2),
(162, '18К3', '50.392826, 30.493716', '50.392958, 30.493561; 50.392985, 30.493735; 50.392698, 30.493859; 50.392669, 30.493676; ', 'images/buildings/Kolovyiskii_18k3.jpg', 'Під''їздів - 2<br>Квартир - 24', 'http://kalinichenko.com.ua/forum/index.php?/forum/40-18%D0%BA3/', 2),
(163, '29/22', '50.393687, 30.492138', '50.393702, 30.491865; 50.393794, 30.492394; 50.393682, 30.492443; 50.393595, 30.491915; ', 'images/buildings/vasilkivska_29-22.JPG', 'Під''їздів - 2<br>Квартир - 40', 'http://kalinichenko.com.ua/forum/index.php?/forum/43-2922/', 4),
(164, '27/1', '50.393825, 30.492993', '50.393814, 30.492564; 50.393943, 30.493358; 50.393837, 30.493399; 50.393707, 30.492605; ', 'images/buildings/vasilkivska_27-1.jpg', 'Під''їздів - 4<br>Квартир - 37', 'http://kalinichenko.com.ua/forum/index.php?/forum/44-271/', 4),
(165, '27К2', '50.393439, 30.492902', '50.393463, 30.492733; 50.393530, 30.493151; 50.393437, 30.493186; 50.393394, 30.492921; 50.393329, 30.492937; 50.393308, 30.492797; ', 'images/buildings/vasilkivska_27k2.jpg', 'Під''їздів - 2<br>Квартир - 24', 'http://kalinichenko.com.ua/forum/index.php?/forum/45-27%D0%BA2/', 4),
(168, '15К3', '50.393343, 30.493512', '50.393464, 30.493377; 50.393490, 30.493521; 50.393227, 30.493634; 50.393202, 30.493488; ', '', 'Під''їздів - 2<br>Квартир - 16', 'http://kalinichenko.com.ua/forum/index.php?/forum/49-15%D0%BA3/', 6),
(169, '25/17', '50.393696, 30.493969', '50.394013, 30.493765; 50.394040, 30.493938; 50.393437, 30.494160; 50.393409, 30.493978; ', 'images/buildings/vasilkivska_25-17.JPG', 'Під''їздів - 4<br>Квартир - 74', 'http://kalinichenko.com.ua/forum/index.php?/forum/50-2517/', 4),
(170, '15', '50.393049, 30.494189', '50.393256, 30.494013; 50.393281, 30.494185; 50.392825, 30.494339; 50.392799, 30.494173; ', 'images/buildings/VZukovsokogo_15.JPG', 'Під''їздів - 1<br>Квартир - 76', 'http://kalinichenko.com.ua/forum/index.php?/forum/51-15/', 6),
(171, '13/16', '50.392348, 30.494427', '50.392649, 30.494237; 50.392673, 30.494374; 50.392047, 30.494631; 50.392023, 30.494500; ', 'images/buildings/VZukovsokogo_13-16.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/52-1316/', 6),
(172, '27К3', '50.393090, 30.493075', '50.393161, 30.492843; 50.393188, 30.493003; 50.393124, 30.493030; 50.393167, 30.493306; 50.393077, 30.493347; 50.393003, 30.492910; ', '', 'Під''їздів - 2<br>Квартир - 18', 'http://kalinichenko.com.ua/forum/index.php?/forum/47-27%D0%BA3/', 4),
(173, '12', '50.392406, 30.493583', '50.392381, 30.493009; 50.392564, 30.494090; 50.392425, 30.494149; 50.392246, 30.493071; ', 'images/buildings/Lomonosova_12.jpg', 'Під''їздів - <br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/53-12/', 1),
(175, '12', '50.392934, 30.495211', '50.392996, 30.495055; 50.393031, 30.495322; 50.392864, 30.495380; 50.392832, 30.495099; ', 'images/buildings/VZukovsokogo_12.jpg', 'Під''їздів - <br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/55-12/', 6),
(176, '10', '50.392479, 30.495104', '50.392774, 30.495034; 50.392761, 30.495203; 50.392155, 30.495158; 50.392164, 30.494985; ', 'images/buildings/VZukovsokogo_10.jpg', 'Під''їздів - 3<br>Квартир - 72', 'http://kalinichenko.com.ua/forum/index.php?/forum/56-10/', 6),
(177, '13', '50.393760, 30.495174', '50.393776, 30.494926; 50.393845, 30.495368; 50.393734, 30.495411; 50.393668, 30.494971; ', 'images/buildings/VDubinina_13.jpg', 'Під''їздів - <br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/57-13/', 5),
(178, '23/16', '50.394089, 30.494681', '50.394074, 30.494259; 50.394231, 30.495238; 50.394117, 30.495272; 50.393987, 30.494487; 50.393815, 30.494554; 50.393787, 30.494380; ', 'images/buildings/vasilkivska_23-16.jpg', 'Під''їздів - 6<br>Квартир - 46', 'http://kalinichenko.com.ua/forum/index.php?/forum/58-2316/', 4),
(179, '14', '50.393331, 30.494792', '50.393569, 30.494473; 50.393624, 30.494633; 50.393093, 30.495086; 50.393042, 30.494945; ', 'images/buildings/VZukovsokogo_14.jpg', 'Під''їздів - 3<br>Квартир - 129', 'http://kalinichenko.com.ua/forum/index.php?/forum/54-14/', 6),
(180, '14', '50.392953, 30.496068', '50.393239, 30.495870; 50.393258, 30.496011; 50.392656, 30.496234; 50.392631, 30.496100; ', 'images/buildings/VDubinina_14.jpg', 'Під''їздів - 5<br>Квартир - 75', 'http://kalinichenko.com.ua/forum/index.php?/forum/46-14/', 5),
(181, '16', '50.393630, 30.496110', '50.393949, 30.495883; 50.393973, 30.496055; 50.393323, 30.496311; 50.393286, 30.496135; ', 'mages/buildings/VDubinina_16.jpg', 'Гуртожиток міліції', 'http://kalinichenko.com.ua/forum/index.php?/forum/59-16/', 5),
(182, '13', '50.393648, 30.496744', '50.393831, 30.496575; 50.393853, 30.496748; 50.393477, 30.496885; 50.393453, 30.496716; ', 'images/buildings/Burmistenka_13.JPG', 'Під''їздів - 3<br>Квартир - 60', 'http://kalinichenko.com.ua/forum/index.php?/forum/61-13/', 7),
(183, '7/14', '50.392276, 30.495793', '50.392438, 30.495638; 50.392459, 30.495816; 50.392125, 30.495941; 50.392101, 30.495775; ', 'images/buildings/VDubinina_7-14.jpg', 'Під''їздів - <br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/62-714/', 5),
(184, '11', '50.393022, 30.496793', '50.393351, 30.496595; 50.393375, 30.496731; 50.392742, 30.496972; 50.392719, 30.496837; ', 'images/buildings/Burmistenka_11.jpg', 'Під''їздів - 4<br>Квартир - 56', 'http://kalinichenko.com.ua/forum/index.php?/forum/63-11/', 7),
(185, '21/18', '50.394188, 30.495649', '50.394294, 30.495592; 50.394241, 30.495820; 50.394069, 30.495729; 50.394117, 30.495497; ', 'images/buildings/vasilkivska_21-18.JPG', 'Під''їздів - 1<br>Квартир - 53', 'http://kalinichenko.com.ua/forum/index.php?/forum/64-2118/', 4),
(186, '19/15', '50.394287, 30.496348', '50.394399, 30.496268; 50.394353, 30.496488; 50.394174, 30.496408; 50.394218, 30.496185; ', 'images/buildings/vasilkivska_19-15.JPG', 'Під''їздів - 1<br>Квартир - 54', 'http://kalinichenko.com.ua/forum/index.php?/forum/65-1915/', 4),
(187, '12', '50.394001, 30.497263', '50.394442, 30.497037; 50.394468, 30.497212; 50.393566, 30.497521; 50.393537, 30.497346; ', 'images/buildings/Burmistenka_12.jpg', 'Під''їздів - 4<br>Квартир - 149', 'http://kalinichenko.com.ua/forum/index.php?/forum/66-12/', 7),
(188, '10', '50.392945, 30.497681', '50.393397, 30.497410; 50.393421, 30.497582; 50.392502, 30.497920; 50.392476, 30.497748; ', 'images/buildings/Burmistenka_10.jpg', 'Під''їздів - 4<br>Квартир - 149', 'http://kalinichenko.com.ua/forum/index.php?/forum/67-10/', 7),
(190, '3', '50.391483, 30.501265', '50.391314, 30.501312; 50.391630, 30.501008; 50.391681, 30.501166; 50.391362, 30.501472; ', 'images/buildings/ABubnova_3.jpg', 'ЖКГ', 'http://kalinichenko.com.ua/forum/index.php?/forum/70-3/', 8),
(191, '7a', '50.388175, 30.489531', '50.388236, 30.489120; 50.388339, 30.489315; 50.388196, 30.489465; 50.388268, 30.489650; 50.388191, 30.489641; 50.388282, 30.489869; 50.388118, 30.489960; 50.387961, 30.489671; 50.388166, 30.489440; 50.388067, 30.489273; ', 'images/buildings/Sechenova_7a.jpg', 'Під''їздів - 1<br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/76-7%D0%B0/', 3),
(192, '5', '50.392013, 30.501482', '50.391752, 30.501538; 50.391798, 30.501489; 50.391806, 30.501510; 50.391852, 30.501470; 50.391870, 30.501512; 50.392101, 30.501297; 50.392082, 30.501250; 50.392132, 30.501201; 50.392116, 30.501160; 50.392161, 30.501114; 50.392229, 30.501274; 50.392208, 30.501296; 50.392242, 30.501386; 50.391871, 30.501731; 50.391843, 30.501663; 50.391810, 30.501672; ', 'images/buildings/ABubnova_5.jpg', 'Під''їздів - 3<br>Квартир - 39', 'http://kalinichenko.com.ua/forum/index.php?/forum/71-5/', 8),
(193, '12/12', '50.392297, 30.496351', '50.392484, 30.496137; 50.392514, 30.496322; 50.392364, 30.496370; 50.392394, 30.496635; 50.392285, 30.496676; 50.392217, 30.496246; ', 'images/buildings/VDubinina_12-12.JPG', 'Під''їздів - 3<br>Квартир - 23', 'http://kalinichenko.com.ua/forum/index.php?/forum/72-1212/', 5),
(194, '9/10', '50.392452, 30.497139', '50.392647, 30.496992; 50.392674, 30.497158; 50.392371, 30.497284; 50.392294, 30.496817; 50.392403, 30.496783; 50.392456, 30.497071; ', 'images/buildings/Burmistenka_9-10.jpg', 'Під''їздів - 3<br>Квартир - 40', 'http://kalinichenko.com.ua/forum/index.php?/forum/73-910/', 7),
(196, '13', '50.394364, 30.499262', '50.394774, 30.498924; 50.394815, 30.499074; 50.393967, 30.499642; 50.393923, 30.499481; ', 'images/buildings/ABubnova_13.jpg', 'Під''їздів - 4<br>Квартир - 143', 'http://kalinichenko.com.ua/forum/index.php?/forum/68-13/', 8),
(197, '11/8', '50.393324, 30.500125', '50.393717, 30.499793; 50.393755, 30.499943; 50.392951, 30.500463; 50.392907, 30.500308; ', 'images/buildings/ABubnova_11-8.jpg', 'Під''їздів - 3<br>Квартир - 162', 'http://kalinichenko.com.ua/forum/index.php?/forum/75-118/', 8),
(198, '5', '50.388137, 30.491213', '50.388047, 30.490785; 50.388347, 30.491570; 50.388263, 30.491650; 50.387956, 30.490872; ', 'images/buildings/Sechenova_5.jpg', 'Під''їздів - 3<br>Квартир - 30', 'http://kalinichenko.com.ua/forum/index.php?/forum/20-5/', 3),
(200, '3', '50.388647, 30.492442', '50.388451, 30.491832; 50.388913, 30.492981; 50.388841, 30.493061; 50.388376, 30.491904; ', 'images/buildings/Sechenova_3.JPG', 'Під''їздів - 6<br>Квартир - 90', 'http://kalinichenko.com.ua/forum/index.php?/forum/19-3/', 3),
(201, '12', '50.394290, 30.500185', '50.394554, 30.499916; 50.394591, 30.500072; 50.394034, 30.500453; 50.393993, 30.500292; ', 'images/buildings/ABubnova_12.jpg', 'Під''їздів - 6<br>Квартир - 104', 'http://kalinichenko.com.ua/forum/index.php?/forum/77-12/', 8),
(202, '10', '50.393870, 30.500769', '50.393822, 30.500378; 50.394015, 30.501076; 50.393920, 30.501141; 50.393724, 30.500447; ', 'images/buildings/ABubnova_10.jpg', 'Під''їздів - 3<br>Квартир - 62', 'http://kalinichenko.com.ua/forum/index.php?/forum/78-10/', 8),
(203, '8', '50.393507, 30.500997', '50.393458, 30.500614; 50.393652, 30.501300; 50.393553, 30.501372; 50.393362, 30.500678; ', 'images/buildings/ABubnova_8.JPG', 'Під''їздів - 3<br>Квартир - 60', 'http://kalinichenko.com.ua/forum/index.php?/forum/79-8/', 8),
(204, '6', '50.393163, 30.501274', '50.393106, 30.500837; 50.393356, 30.501730; 50.393247, 30.501805; 50.393000, 30.500907; ', 'images/buildings/ABubnova_6.jpg', 'Під''їздів - 4<br>Квартир - 85', 'http://kalinichenko.com.ua/forum/index.php?/forum/80-6/', 8),
(205, '13', '50.394890, 30.500917', '50.394847, 30.500507; 50.395008, 30.501301; 50.394905, 30.501352; 50.394748, 30.500558; ', 'images/buildings/vasilkivska_13.JPG', 'Під''їздів - 3<br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/81-13/', 4),
(206, '11', '50.392004, 30.496630', '50.392004, 30.496288; 50.392110, 30.496974; 50.392021, 30.497009; 50.391911, 30.496330; ', 'images/buildings/Lomonosova_11.jpg', 'Під''їздів - 3<br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/82-11/', 1),
(207, '7/11', '50.391852, 30.497382', '50.392163, 30.497185; 50.392185, 30.497338; 50.391553, 30.497567; 50.391531, 30.497414; ', 'images/buildings/Burmistenka_7-11.jpg', 'Під''їздів - 4<br>Квартир - 76', 'http://kalinichenko.com.ua/forum/index.php?/forum/83-711/', 7),
(208, '6/9', '50.392019, 30.497976', '50.392240, 30.497810; 50.392267, 30.497979; 50.391801, 30.498158; 50.391773, 30.497989; ', 'images/buildings/Burmistenka_6-9.jpg', 'Під''їздів - <br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/84-69/', 7),
(209, '6К1', '50.391435, 30.498191', '50.391655, 30.498017; 50.391679, 30.498191; 50.391210, 30.498362; 50.391191, 30.498191; ', 'images/buildings/Burmistenka_6k-1.jpg', 'Під''їздів - 3<br>Квартир - 60', 'http://kalinichenko.com.ua/forum/index.php?/forum/85-6%D0%BA1/', 7),
(210, '6К2', '50.391150, 30.498958', '50.391445, 30.498768; 50.391469, 30.498920; 50.390857, 30.499147; 50.390830, 30.498999; ', 'images/buildings/Burmistenka_6k-2.jpg', 'Під''їздів - 4<br>Квартир - 80', 'http://kalinichenko.com.ua/forum/index.php?/forum/86-6%D0%BA2/', 7),
(211, '8/9', '50.391841, 30.498722', '50.392076, 30.498548; 50.392090, 30.498696; 50.391612, 30.498867; 50.391590, 30.498726; ', 'images/buildings/Burmistenka_8-9.jpg', 'Під''їздів - 3<br>Квартир - 60', 'http://kalinichenko.com.ua/forum/index.php?/forum/87-89/', 7),
(212, '3', '50.390766, 30.497760', '50.391068, 30.497585; 50.391090, 30.497737; 50.390456, 30.497946; 50.390437, 30.497794; ', 'images/buildings/Burmistenka_3.jpg', 'Під''їздів - 4<br>Квартир - 70', 'http://kalinichenko.com.ua/forum/index.php?/forum/88-3/', 7),
(213, '4', '50.390593, 30.496858', '50.390752, 30.496731; 50.390775, 30.496903; 50.390455, 30.497025; 50.390429, 30.496844; ', 'images/buildings/VDubinina_4.jpg', 'Під''їздів - 2<br>Квартир - 24', 'http://kalinichenko.com.ua/forum/index.php?/forum/89-4/', 5),
(216, '3', '50.391351, 30.496028', '50.391459, 30.495939; 50.391475, 30.496047; 50.391245, 30.496133; 50.391229, 30.496031; ', 'images/buildings/VDubinina_3.jpg', 'Під''їздів - 2<br>Квартир - 8', 'http://kalinichenko.com.ua/forum/index.php?/forum/91-3/', 5),
(217, '4', '50.393496, 30.502523', '50.393472, 30.502209; 50.393626, 30.502770; 50.393523, 30.502840; 50.393369, 30.502287; ', 'images/buildings/Lomonosova_4.jpg', 'Під''їздів - 2<br>Квартир - 46', 'http://kalinichenko.com.ua/forum/index.php?/forum/74-4/', 1),
(218, '1', '50.390947, 30.496278', '50.391080, 30.496130; 50.391111, 30.496317; 50.390806, 30.496431; 50.390776, 30.496246; ', 'images/buildings/VDubinina_1.jpg', 'Під''їздів - 1<br>Квартир - 9', 'http://kalinichenko.com.ua/forum/index.php?/forum/90-1/', 5),
(219, '6', '50.391117, 30.496710', '50.391267, 30.496573; 50.391290, 30.496744; 50.390943, 30.496854; 50.390922, 30.496687; ', 'images/buildings/VDubinina_6.jpg', 'Під''їздів - <br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/92-6/', 5),
(220, '5/15', '50.391790, 30.495971', '50.391948, 30.495822; 50.391975, 30.496000; 50.391642, 30.496120; 50.391617, 30.495946; ', 'images/buildings/VDubinina_5-15.jpg', 'Під''їздів - <br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/93-515/', 5),
(221, '8/17', '50.391725, 30.495211', '50.391824, 30.495111; 50.391845, 30.495238; 50.391627, 30.495314; 50.391606, 30.495190; ', 'images/buildings/VZukovsokogo_8-17.jpg', 'Під''їздів - 2<br>Квартир - 12', 'http://kalinichenko.com.ua/forum/index.php?/forum/94-817/', 6),
(222, '5', '50.389872, 30.494699', '50.389759, 30.494270; 50.390067, 30.495043; 50.389987, 30.495129; 50.389673, 30.494352; ', '', 'Під''їздів - 4<br>Квартир - 64', 'http://kalinichenko.com.ua/forum/index.php?/forum/95-5/', 6),
(223, '3К1', '50.389845, 30.495565', '50.389958, 30.495357; 50.390014, 30.495512; 50.389736, 30.495782; 50.389678, 30.495638; ', 'images/buildings/VZukovsokogo_3k1.jpg', 'Під''їздів - 2<br>Квартир - 30', 'http://kalinichenko.com.ua/forum/index.php?/forum/96-3%D0%BA1/', 6),
(224, '3К2', '50.389591, 30.494928', '50.389479, 30.494519; 50.389767, 30.495228; 50.389683, 30.495304; 50.389404, 30.494598; ', '', 'Під''їздів - 4<br>Квартир - 60', 'http://kalinichenko.com.ua/forum/index.php?/forum/97-3%D0%BA2/', 6),
(225, '4', '50.390670, 30.498660', '50.391079, 30.498266; 50.391113, 30.498482; 50.390851, 30.498582; 50.390869, 30.498700; 50.390486, 30.498853; 50.390460, 30.498718; 50.390213, 30.498812; 50.390184, 30.498600; 50.390480, 30.498497; 50.390500, 30.498614; 50.390800, 30.498501; 50.390779, 30.498384; ', 'images/buildings/Burmistenka_4.jpg', 'Гуртожиток №4', 'http://kalinichenko.com.ua/forum/index.php?/forum/98-4/', 7),
(227, '112', '50.386856, 30.490379', '50.386622, 30.489960; 50.386704, 30.489883; 50.387083, 30.490821; 50.387008, 30.490907; ', 'images/buildings/40richyyz_112.JPG', 'Під''їздів - 4<br>Квартир - 105', 'http://kalinichenko.com.ua/forum/index.php?/forum/100-112/', 9),
(229, '110', '50.387400, 30.491843', '50.387219, 30.491182; 50.387656, 30.492292; 50.387601, 30.492349; 50.387614, 30.492400; 50.387551, 30.492448; 50.387282, 30.491766; 50.387321, 30.491724; 50.387142, 30.491252; ', 'images/buildings/40richyyz_110.jpg', 'Під''їздів - 4<br>Квартир - 118', 'http://kalinichenko.com.ua/forum/index.php?/forum/101-110/', 9),
(230, '108К3', '50.387738, 30.492943', '50.387773, 30.492723; 50.387892, 30.493007; 50.387797, 30.493103; 50.387797, 30.493130; 50.387721, 30.493195; 50.387595, 30.492857; 50.387663, 30.492792; 50.387674, 30.492808; ', 'images/buildings/40richyyz_108k3.jpg', 'Під''їздів - 1<br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/102-108%D0%BA3/', 9),
(231, '3/1', '50.388818, 30.493630', '50.389070, 30.493289; 50.389121, 30.493422; 50.388516, 30.494010; 50.388465, 30.493877; ', 'images/buildings/Kolovyiskii_3-1.jpg', 'Під''їздів - 4<br>Квартир - 104', 'http://kalinichenko.com.ua/forum/index.php?/forum/103-31/', 2),
(232, '7К1', '50.392323, 30.498601', '50.392307, 30.498186; 50.392433, 30.498926; 50.392331, 30.498958; 50.392209, 30.498231; ', 'images/buildings/Lomonosova_7k-1.jpg', 'Під''їздів - 3<br>Квартир - 63', 'http://kalinichenko.com.ua/forum/index.php?/forum/104-7%D0%BA1/', 1),
(233, '5/1', '50.392486, 30.499526', '50.392470, 30.499143; 50.392596, 30.499870; 50.392494, 30.499915; 50.392372, 30.499187; ', 'images/buildings/Lomonosova_5-1.jpg', 'Під''їздів - 4<br>Квартир - 80', 'http://kalinichenko.com.ua/forum/index.php?/forum/105-51/', 1),
(234, '5А', '50.392002, 30.500125', '50.392246, 30.499781; 50.392299, 30.499928; 50.391766, 30.500444; 50.391705, 30.500291; ', 'images/buildings/ABubnova_5a.jpg', 'Під''їздів - <br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/106-5%D0%B0/', 8),
(235, '7', '50.392067, 30.500680', '50.392197, 30.500470; 50.392254, 30.500642; 50.391957, 30.500910; 50.391896, 30.500744; ', 'images/buildings/ABubnova_7.jpg', 'Під''їздів - 1<br>Квартир - ', 'http://kalinichenko.com.ua/forum/index.php?/forum/107-7/', 8),
(236, '94', '50.391352, 30.500685', '50.391564, 30.500383; 50.391622, 30.500518; 50.391096, 30.501005; 50.391047, 30.500874; ', 'images/buildings/40richyyz_94.jpg', 'Під''їздів - 4<br>Квартир - 68', 'http://kalinichenko.com.ua/forum/index.php?/forum/108-94/', 9),
(237, '94/96', '50.391254, 30.499720', '50.391248, 30.499359; 50.391357, 30.500080; 50.391257, 30.500116; 50.391145, 30.499399; ', 'images/buildings/40richyyz_94-96.jpg', 'Під''їздів - 5<br>Квартир - 80', 'http://kalinichenko.com.ua/forum/index.php?/forum/109-9496/', 9),
(238, '96', '50.390762, 30.499945', '50.390644, 30.499521; 50.390984, 30.500351; 50.390891, 30.500450; 50.390552, 30.499620; ', 'images/buildings/40richyyz_96.jpg', 'Під''їздів - 4<br>Квартир - 66', 'http://kalinichenko.com.ua/forum/index.php?/forum/110-96/', 9);

-- --------------------------------------------------------

--
-- Структура таблицы `checked_requests`
--

CREATE TABLE IF NOT EXISTS `checked_requests` (
  `REQUEST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) NOT NULL,
  `PHONE` varchar(20) NOT NULL,
  `EMAIL` varchar(200) NOT NULL,
  `TITLE` varchar(1000) NOT NULL,
  `STREET` int(10) unsigned NOT NULL,
  `BUILDING` int(10) unsigned NOT NULL,
  `MESSAGE` mediumtext NOT NULL,
  `REQUEST_DATE` date NOT NULL,
  PRIMARY KEY (`REQUEST_ID`),
  KEY `STREET` (`STREET`),
  KEY `BUILDING` (`BUILDING`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `done_news`
--

CREATE TABLE IF NOT EXISTS `done_news` (
  `ID` int(3) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) NOT NULL,
  `IMAGE` int(4) NOT NULL,
  `NEWS_TEXT` mediumtext NOT NULL,
  `NEWS_DATE` date NOT NULL,
  `AUTHOR` varchar(30) NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`ID`),
  KEY `IMAGE` (`IMAGE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Структура таблицы `done_news_archive`
--

CREATE TABLE IF NOT EXISTS `done_news_archive` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) NOT NULL,
  `IMAGE` int(4) NOT NULL,
  `NEWS_TEXT` mediumtext NOT NULL,
  `NEWS_DATE` date NOT NULL,
  `AUTHOR` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IMAGE` (`IMAGE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `done_news_archive`
--

INSERT INTO `done_news_archive` (`ID`, `TITLE`, `IMAGE`, `NEWS_TEXT`, `NEWS_DATE`, `AUTHOR`) VALUES
(1, 'asd', 14, 'asd', '2015-12-22', ''),
(2, 'asd', 14, 'qwe', '2015-12-22', ''),
(3, 'Title1', 14, 'Text\ntext\ntext', '2016-01-21', ''),
(4, 'Done', 14, 'Line1\nLine2\nLine3\nLine4asdasdasdasdddddddddddddddddddddddddd', '2016-01-22', ''),
(5, 'Done', 14, 'qweqw\nwqe\n\nqw\nq\nq\neqw\n', '2016-01-22', ''),
(6, 'test', 21, 'test', '2016-01-25', ''),
(7, 'test1', 21, 'test1', '2016-01-25', ''),
(8, 'test_news1', 21, 'In addition to bottom-popup surveys, LeadConverter offers several different types of popups including top-of-the-page, over the page, discount offers and more. It even enables you to engage website visitors directly using a dialog popup similar to the one shown below. Add in targeted messaging, browsing history tracking for each visitor and additional analytic and this tool provides a variety of insights to your business.', '2016-01-25', ''),
(9, 'test_news3', 21, 'In addition to bottom-popup surveys, LeadConverter offers several different types of popups including top-of-the-page, over the page, discount offers and more. It even enables you to engage website visitors directly using a dialog popup similar to the one shown below. Add in targeted messaging, browsing history tracking for each visitor and additional analytic and this tool provides a variety of insights to your business.', '2016-01-25', '');

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `IMAGE_ID` int(4) NOT NULL AUTO_INCREMENT,
  `LINK` varchar(150) NOT NULL,
  `IMAGE_TYPE` int(2) unsigned NOT NULL,
  PRIMARY KEY (`IMAGE_ID`),
  KEY `IMAGE_TYPE` (`IMAGE_TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`IMAGE_ID`, `LINK`, `IMAGE_TYPE`) VALUES
(15, '/home/knztrquq/public_html/images/icons/constructor2.png', 2),
(16, '/home/knztrquq/public_html/images/icons/constructor4 (2).png', 2),
(17, '/home/knztrquq/public_html/images/icons/photo_2015-12-28_00-09-03.jpg', 2),
(18, '/home/knztrquq/public_html/images/icons/pipes1.png', 2),
(20, '/home/knztrquq/public_html/images/icons/wall20 (1).png', 2),
(21, '/home/knztrquq/public_html/images/newsIcons/61_tn.jpg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `image_types`
--

CREATE TABLE IF NOT EXISTS `image_types` (
  `TYPE_ID` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `IMAGE_TYPE` varchar(20) NOT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `image_types`
--

INSERT INTO `image_types` (`TYPE_ID`, `IMAGE_TYPE`) VALUES
(1, 'newsIcons'),
(2, 'icons');

-- --------------------------------------------------------

--
-- Структура таблицы `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(2, '1450219315'),
(2, '1450219377'),
(2, '1450219406'),
(1, '1450367202'),
(1, '1450367474'),
(1, '1450367513'),
(1, '1450367637'),
(3, '1450379096'),
(3, '1450380530'),
(3, '1450380635');

-- --------------------------------------------------------

--
-- Структура таблицы `markers`
--

CREATE TABLE IF NOT EXISTS `markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `lat` text NOT NULL,
  `lng` text NOT NULL,
  `imageid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`) VALUES
(1, 'test_user', 'test@example.com', '00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc', 'f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef'),
(2, 'admin', 'admin@email.com', 'c8cac6f86f8ac85bf238eb2005bd328a6d25b86bbf9bcbae8679524d36ab0340ee974de3fef093f945cfbde64b6f25339e1e7912c3e7c2d8bcf85546f3cb46c5', '8f14b6c679bafae08dcdebce6254ced595ec9f2d099157e2b87c2f294550f4fdf5bc4f14ae3d18afb64a1d4b161213c93dc7e9dc63c361b877e2d4e520a8f93a'),
(3, 'test1', 'test1@example.com', 'cd7064b7f0fd426885ea05b51fa3cd5b3cff4cd777ff3a3867fe3c4d5c817e99aa5b957bad33900c8bdf631ffc7fbde41699fbdfe6dba5cb8b3bdee43117ae9a', 'ba382fe77fbd3d84cf3d36dacf38f585f40c385aecf9d8f78c3bb90d99b1de89c56a019f22afa906d50c4b126111f8747e771b5d59e5a90c2b7cc29dec6b36be');

-- --------------------------------------------------------

--
-- Структура таблицы `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `photo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(150) NOT NULL,
  `description` varchar(200) NOT NULL,
  `album` int(10) unsigned NOT NULL,
  PRIMARY KEY (`photo_id`),
  UNIQUE KEY `link` (`link`),
  KEY `album` (`album`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `photos`
--

INSERT INTO `photos` (`photo_id`, `link`, `description`, `album`) VALUES
(27, '/home/knztrquq/public_html/images/albums/3/1.jpg', 'Фото №1', 30),
(28, '/home/knztrquq/public_html/images/albums/3/2.jpg', 'Фото №2', 30),
(29, '/home/knztrquq/public_html/images/albums/3/3.jpg', 'Фото №3', 30),
(30, '/home/knztrquq/public_html/images/albums/4/4.jpg', 'Фото №4', 31),
(31, '/home/knztrquq/public_html/images/albums/4/5.jpg', 'Фото №5', 31),
(32, '/home/knztrquq/public_html/images/albums/4/6.jpg', 'Київрада', 31),
(33, '/home/knztrquq/public_html/images/albums/4/7.jpg', 'Фото №7', 31);

-- --------------------------------------------------------

--
-- Структура таблицы `planned_news`
--

CREATE TABLE IF NOT EXISTS `planned_news` (
  `ID` int(3) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) NOT NULL,
  `IMAGE` int(4) NOT NULL,
  `NEWS_TEXT` text NOT NULL,
  `NEWS_DATE` date NOT NULL,
  `AUTHOR` varchar(30) NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`ID`),
  KEY `IMAGE` (`IMAGE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Структура таблицы `planned_news_archive`
--

CREATE TABLE IF NOT EXISTS `planned_news_archive` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) NOT NULL,
  `IMAGE` int(4) NOT NULL,
  `NEWS_TEXT` mediumtext NOT NULL,
  `NEWS_DATE` date NOT NULL,
  `AUTHOR` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IMAGE` (`IMAGE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `planned_news_archive`
--

INSERT INTO `planned_news_archive` (`ID`, `TITLE`, `IMAGE`, `NEWS_TEXT`, `NEWS_DATE`, `AUTHOR`) VALUES
(1, 'Title', 14, 'Denya pes', '2016-01-21', ''),
(2, 'Ремонт в гуртожиках', 14, 'З лютого розпочнеться ремонт в гуртожиках №16 і №17!', '2016-01-23', ''),
(3, 'Відновлення водопостачання', 14, 'В зв''язку з аварією було відключене водопостачання по вулиці Сєченова. Для рішення проблеми були задіяні дві бригади ремнотників. На даний час проблема виявлена і буде ліквідована протягом декількох днів. Ресурси на даний ремонт були виділені міського бюджету.', '2016-01-23', ''),
(4, '111', 14, '111', '2016-01-23', ''),
(5, 'test', 21, 'Update : In your example (given link), you have loaded jQuery three times and most importantly, you''ve loaded the plugin (bPopup) more than once and at last jQuery again, so jQuery object changed. So, just load a single verion of jQuery first and then load jquery.easing.js and then load bPopup.js (min/expanded). Also load stylesheet (style.js) with styles declared for your popup div', '2016-01-25', ''),
(6, 'test_news2', 21, 'In addition to bottom-popup surveys, LeadConverter offers several different types of popups including top-of-the-page, over the page, discount offers and more. It even enables you to engage website visitors directly using a dialog popup similar to the one shown below. Add in targeted messaging, browsing history tracking for each visitor and additional analytic and this tool provides a variety of insights to your business.', '2016-01-25', '');

-- --------------------------------------------------------

--
-- Структура таблицы `streets`
--

CREATE TABLE IF NOT EXISTS `streets` (
  `STREET_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`STREET_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `streets`
--

INSERT INTO `streets` (`STREET_ID`, `NAME`) VALUES
(1, 'Вулиця Михайла Ломоносова'),
(2, 'Провулок Коломийський'),
(3, 'Вулиця Сєченова'),
(4, 'Вулиця Васильківська'),
(5, 'Вулиця Володі Дубініна'),
(6, 'Вулиця Василя Жуковського'),
(7, 'Вулиця Бурмистенка'),
(8, 'Вулиця Андрія Бубнова'),
(9, 'Проспект 40-річчя Жовтня');

-- --------------------------------------------------------

--
-- Структура таблицы `unchecked_requests`
--

CREATE TABLE IF NOT EXISTS `unchecked_requests` (
  `REQUEST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) NOT NULL,
  `PHONE` varchar(20) NOT NULL,
  `EMAIL` varchar(200) NOT NULL,
  `STREET` int(10) unsigned NOT NULL,
  `BUILDING` int(10) unsigned NOT NULL,
  `MESSAGE` mediumtext NOT NULL,
  `REQUEST_DATE` date NOT NULL,
  PRIMARY KEY (`REQUEST_ID`),
  KEY `BUILDING` (`BUILDING`),
  KEY `STREET` (`STREET`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `buildings`
--
ALTER TABLE `buildings`
  ADD CONSTRAINT `buildings_ibfk_1` FOREIGN KEY (`STREET`) REFERENCES `streets` (`STREET_ID`);

--
-- Ограничения внешнего ключа таблицы `checked_requests`
--
ALTER TABLE `checked_requests`
  ADD CONSTRAINT `checked_requests_ibfk_1` FOREIGN KEY (`STREET`) REFERENCES `streets` (`STREET_ID`),
  ADD CONSTRAINT `checked_requests_ibfk_2` FOREIGN KEY (`BUILDING`) REFERENCES `buildings` (`BUILDING_ID`);

--
-- Ограничения внешнего ключа таблицы `done_news`
--
ALTER TABLE `done_news`
  ADD CONSTRAINT `done_news_ibfk_1` FOREIGN KEY (`IMAGE`) REFERENCES `images` (`IMAGE_ID`);

--
-- Ограничения внешнего ключа таблицы `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`IMAGE_TYPE`) REFERENCES `image_types` (`TYPE_ID`);

--
-- Ограничения внешнего ключа таблицы `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`album`) REFERENCES `albums` (`album_id`);

--
-- Ограничения внешнего ключа таблицы `planned_news`
--
ALTER TABLE `planned_news`
  ADD CONSTRAINT `planned_news_ibfk_1` FOREIGN KEY (`IMAGE`) REFERENCES `images` (`IMAGE_ID`);

--
-- Ограничения внешнего ключа таблицы `unchecked_requests`
--
ALTER TABLE `unchecked_requests`
  ADD CONSTRAINT `unchecked_requests_ibfk_1` FOREIGN KEY (`STREET`) REFERENCES `streets` (`STREET_ID`),
  ADD CONSTRAINT `unchecked_requests_ibfk_2` FOREIGN KEY (`BUILDING`) REFERENCES `buildings` (`BUILDING_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
