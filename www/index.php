<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Округ Калініченка Дмитра</title>

    <link href="css/head/custom.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="css/news/bootstrap.custom.css" rel="stylesheet">-->
    <!--<link href="css/news/bootstrap-theme.custom.css" rel="stylesheet">-->
    <link href="css/news/site.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/jquery.js"></script>

    <link rel="stylesheet" href="css/footer/footer-distributed-with-address-and-phones.css">
    <script>



    </script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>


    <![endif]-->
    <style>

        #map {
            height: 500px;
            width: 100%;
        }
    </style>
</head>

<body onload="initMap(14); getStreets(); ">
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Головна</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="forum/">Форум</a>
                </li>
                <li>
                    <a href="fullmap.html">Повна карта</a>
                </li>
                <li>
                    <a href="about.html">Команда Калініченка</a>
                </li>
                <li>
                    <a href="directory.html">Довідник</a>
                </li>
                <li>
                    <a href="gallery.html">Галерея</a>
                </li>
                <li>
                    <a href="info.html">Корисна інформація</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Header Carousel -->
<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('images/carusel/carusel7.jpg');"></div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('images/carusel/carusel4.jpeg');"></div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('images/carusel/carusel5.jpg');"></div>
        </div>
    </div>
</header>
<br>
<div class="row">
    <div class="col-md-6" id="news_block">
        <div class="row">
            <div class="col-md-12 actualnewsbody" >
                <div class="panel panel-default">
                    <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Актуальні новини</b></div>
                    <div class="panel-body actual-news-panel-body">
                        <div class="row">
                            <div class="col-xs-12" id="pb">
                                <ul class="demo1" id="actual_news" style="z-index: 2;position: relative;" ></ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer"  style="z-index: 10;position: relative;" > </div>
                </div>
            </div>
        </div>
<!--        <br><br><br><br>-->
        <div class="row">
        <div class="col-md-6 newsbody" style="height: 100%;">
            <div class="panel panel-default">
                <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Виконані роботи</b></div>
                <div class="panel-body pd-news-panel-body" >
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="demo1" id="done_news" ></ul>
                        </div>
                    </div>
                </div>
                <div class="panel-footer"> </div>
            </div>
        </div>
        <div class="col-md-6 newsbody" style="height: 100%;">
            <div class="panel panel-default">
                <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Заплановані роботи</b></div>
                <div class="panel-body pd-news-panel-body" id="newspanel">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="demo1" id="planned_news" ></ul>
                        </div>
                    </div>
                </div>
                <div class="panel-footer"> </div>
            </div>
        </div>
        </div>
    </div>
    <div class="col-md-6" id="mapbody">
        <div class="panel panel-default">
            <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Мапа округу Калініченка Дмитра</b></div>
            <div class="panel-body">
                <div class="map" id="map"></div>
                <br>
                <form name="openMap" novalidate>
                    <button type="button" style="margin-left:20px;" onclick="location.href='fullmap.html';" class="btn btn-primary">Розширити карту на повний екран</button>
                </form>
                <br>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 problem-form">
        <div class="panel panel-default" style="overflow: hidden">
            <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Повідомити про проблему</b></div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Ваше ім'я:</label>
                    <input oninput="checkName()" type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                    <p id="incorrectName" class="help-block" style="color: red"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Номер телефону: </label>
                    <input onchange="checkPhone()" type="text" class="form-control" id="phone" required data-validation-required-message="Please enter your phone.">
                    <p id="incorrectPhone" class="help-block" style="color: red"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>E-mail(не обов'язково): </label>
                    <input onchange="checkMail()" type="email" class="form-control" id="email">
                    <p id="incorrectMail" class="help-block" style="color: red"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Вулиця:</label>
                    <select id="street" class="form-control select2-dropdown-open" onchange="loadBuildings();checkStreet()">
                        <option value="" id="delete"></option>
                    </select>
                    <p id="incorrectStreet" class="help-block" style="color: red"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Будинок:</label>
                    <select id="building" class="form-control" onchange="checkBuilding()">
                    </select>
                    <p id="incorrectBuilding" class="help-block" style="color: red"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Повідомлення:</label>
                    <textarea rows="9" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
                    <p id="incorrectMessage" class="help-block" style="color: red"></p>
                </div>
            </div>
            <div id="success"></div>
            <!-- For success/fail messages -->
            <button type="submit" style="margin: 0px 15px 5px 10px; float: right" class="btn btn-primary" onclick="sendMessage()">Відправити повідомлення</button>
            <!--        </form>-->
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>Останні новини Києва та району</b></div>
            <link rel="stylesheet" href="http://www.segodnya.ua/user/css/informer_styles.css" type="text/css" />
            <div class="informers-layout">
                <div class="inf-block">
                    <div class="inf-body kyiv-news">
                        <div id="segodnya_ua_inner">Завантаження...</div><div class="clr"></div>
                        <div class="inf-item last-inf-item"><a class="i-left inf-links" target="_blank" rel="nofollow" href="http://ukr.segodnya.ua/allnews.html" title="Усi новини">Усi новини</a><!--<a class="inf-links i-right" href="http://ukr.segodnya.ua" rel="nofollow" title="ukr.segodnya.ua">Ukr.Segodnya.ua</a>--><div class="clr"></div></div>
                    </div>
                </div>
            </div>
            <script src="http://www.segodnya.ua/user/informers/87abf4014cecac19c298d39eb381d24c.js"></script>
        </div>
    </div>

</div>
<hr>


<!--PopUp-->
<div id="popup"></div>

<!--<hr>-->

<footer class="footer-distributed">
    <div class="footer-left">
        <p class="footer-links">
            <a href="forum/">Форум</a>
            ·
            <a href="fullmap.html">Повна карта</a>
            ·
            <a href="about.html">Команда Калініченка</a>
            ·
            <a href="directory.html">Довідник</a>
            ·
            <a href="gallery.html">Галерея</a>
            ·
            <a href="info.html">Корисна інформація</a>
        </p>
    </div>
    <div class="footer-center">
        <div>
            <i class="fa fa-map-marker"></i>
            <p><span>Проспект Голосіївський, 96, кабінет 126</span>Київ</p>
        </div>
        <div>
            <i class="fa fa-phone"></i>
            <p>(044) 259 71 24</p>
        </div>
        <div>
            <i class="fa fa-envelope"></i>
            <p><a href="mailto:okrug.kalinichenko@gmail.com">okrug.kalinichenko@gmail.com</a></p>
        </div>
    </div>
    <div class="footer-right">
        <p class="footer-company-about"> Ми в соц. мережах:</p>
        <div class="footer-icons">
            <a href="https://www.facebook.com/dmitro.kalinichenko"><i class="fa fa-facebook"></i></a>
            <a href="http://vk.com/dmitro.kalinichenko"><i class="fa fa-vk"></i></a>
        </div>
        <br><p class="footer-company-name">Всі права захищено &copy; 2015</p>
        <br><br><p class="footer-company-name">Знайшли помилку? Звертайтесь на адресу:</p>
        <br><p><font color="#fdf5e6">admin@kalinichenko.com.ua</font> </p>
    </div>

</footer>



<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<script src="js/jquery.bootstrap.newsbox.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Script to Activate the Carousel -->
<script>

</script>

<script type="text/javascript" src="js/custom/indexGalleryRand.js"></script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdGBuQLD_rjbltNmw0QI4PrIHvM0-SyVc&signed_in=true"></script>

<script src="js/custom/googlemap.js" type="text/javascript"></script>
<script src="js/custom/requests.js" type="text/javascript"></script>
<script src="js/custom/polygons.js" type="text/javascript"></script>
<script src="js/custom/news.js" type="text/javascript"></script>
<script src="js/custom/indexScripts.js" type="text/javascript"></script>
</body>

</html>
