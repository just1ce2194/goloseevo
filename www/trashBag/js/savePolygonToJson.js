function saveToJson() {
    var regex = /([0-9]+[.|,][0-9])|([0-9][.|,][0-9]+)|([0-9]+)/g;
    var street = $('#street').val();
    var building = $('#building').val();
    var coords = $('[name="coords[]"]');
    var newCoords = [];
    var j = 0;
    for (var i = 0; i < coords.length; i++) {
        var coord = coords[i].value.split(", ");
        if (coord.length != 2) {
            alert("Input1 error");
            return;
        }
        /* if (regex.test(coord[0])===false) {
         alert("Input2 error");
         alert(regex.test(coord[0])===false);
         alert(regex.test(coord[0]));
         return;
         }
         if (regex.test(coord[1])===false) {
         alert("Input3 error");
         return;
         }*/
        newCoords[j++] = [parseFloat(coord[0].replace(',', '.')), parseFloat(coord[1].replace(',', '.'))];
    }
    var center = $('#center').val();
    var newCenter = center.split(", ");
    if (newCenter.length != 2) {
        alert("Input4 error");
        return;
    }
    /*if (regex.test(newCenter[0])===false) {
     alert("Input5 error");
     return;
     }
     if (regex.test(newCenter[1])===false) {
     alert("Input6 error");
     return;
     }*/

    center = [parseFloat(newCenter[0].replace(',', '.')), parseFloat(newCenter[1].replace(',', '.'))];
    var picture = $('#picture').val();
    var info = $('#info').val();
    var link = $('#link').val();

    var request = new XMLHttpRequest();
    request.open("POST", "/data/newMapObj.json", false);
    request.send(null);
    var my_JSON_object = JSON.parse(request.responseText);
    var streets = my_JSON_object["streets"];

    var flagStreet = false;

    var content = new Object();
    content.number = building;
    content.center = center;
    content.coordinates = newCoords;
    content.picture = picture;
    content.info = info;
    content.linktoforum = link;
    var jsonString;


    streets.forEach(function (street1) {
        if (street === street1["name"]) {
            flagStreet = true;
            street1["buildings"].push(content);
            jsonString = JSON.stringify(my_JSON_object);
        }
    });
    if (flagStreet === false) {
        var str = new Object();
        str.name = street;
        str.buildings = [];
        str.buildings.push(content);
        streets.push(str);
        jsonString = JSON.stringify(my_JSON_object);
    }
    req.open("POST", "JSON_Handler.php", true);
    req.setRequestHeader("Content-type", "application/json");
    req.send(jsonString);
}