function checkAccordance(){
    var request = new XMLHttpRequest();
    request.open("POST", "/php/getBuildingsStreets.php", false);
    request.send(null);
    var res = JSON.parse(request.responseText);

    request.open("POST", "/php/getForumBuildingStreets.php", false);
    request.send(null);
    var res1 = JSON.parse(request.responseText);
    if (res.length != res1.length){
        alert("Кількість записів в БД не співпадає з кількістю тем на форумі.");
        return;
    }
    var dbStreets = [];
    var dbBuildings = [];
    for (var i=0;i<res.length;i++){
        dbStreets.push(res[i]['street']);
        dbBuildings.push(res[i]['building']);
    }
    var forumStreets = [];
    var forumBuildings = [];
    for (var j=0;j<res.length;j++){
        forumStreets.push(res1[j][0]);
        forumBuildings.push(res1[j][1]);
    }
    dbStreets.sort();
    dbBuildings.sort();
    forumStreets.sort();
    forumBuildings.sort();
    for (var k=0;k<dbBuildings.length;k++){
        if (dbStreets[k]!=forumStreets[k]){
            alert("Вулиці \""+dbStreets[k]+"\" в базі даних немає відповідної теми на форумі");
            return;
        }
        if (dbBuildings[k]!=forumBuildings[k]){
            alert("Для будинка \""+dbBuildings[k]+"\" в базі даних немає відповідної теми на форумі");
            return;
        }
    }
    alert("Buildings and streets match.")
}