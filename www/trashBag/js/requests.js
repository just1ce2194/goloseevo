function getRequests(){
    var request = new XMLHttpRequest();
    request.open("POST", "/php/getUncheckedRequests.php", false);
    request.send(null);

    var res = JSON.parse(request.responseText);
    $('#divtable').empty();
    if (res.length!=0) {
        var table = $('<table id="tableid" cellpadding="12"></table>');
        $('#divtable').append(table);
        $('#tableid').append($('<tr><th>№</th><th>Ім"я відправника</th><th>Дата</th><th>Вулиця</th><th>Будинок</th><th>Повідомлення</th><th>Заголовок</th></tr>'));
        for (var i = 0; i < res.length; i++) {
            $('#tableid').append($('<tr id="tr' + i + '"></tr>'));
            $('#tr' + i).append($('<td>' + (i + 1) + '</td>'));
            $('#tr' + i).append($('<td hidden id="tdid' + i + '">' + res[i]['id'] + '</td>'));
            $('#tr' + i).append($('<td id="tdname' + i + '">' + res[i]['name'] + '</td>'));
            $('#tr' + i).append($('<td id="tddate' + i + '">' + res[i]['date'] + '</td>'));
            $('#tr' + i).append($('<td id="tdstreet' + i + '">' + res[i]['street'] + '</td>'));
            $('#tr' + i).append($('<td id="tdbuilding' + i + '">' + res[i]['building'] + '</td>'));
            $('#tr' + i).append($('<td><textarea rows="10" cols="50" id="tdmessage' + i + '">' + res[i]['message']+"\n\r\n\rАвтор: "+ res[i]['name'] + '</textarea></td>'));
            $('#tr' + i).append($('<td><input id="tdtitle' + i + '" type="text" placeholder="Заголовок"/></td>'));
            $('#tr' + i).append($('<td><button id="sendbutton' + (i) + '" onclick="sendRequestToForum(this)">' + 'Send to forum' + '</button></td>'));
            $('#tr' + i).append($('<td><button id="deletebutton' + (i) + '" onclick="deleteRequestFromDB(this)">' + 'Delete' + '</button></td>'));
        }
    }
}

function deleteRequestFromDB(e){
    var i = e.id.replace('deletebutton','');
    var id = $('#tdid'+i).text();

    var content = new Object();
    content.id = id;

    var request = new XMLHttpRequest();
    request.open("POST", "/php/deleteRequestFromDB.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==="SUCCESS"){
        getRequests();
    }
    else{
        alert("DATABASE ERROR!");
    }
}

function sendRequestToForum(e){
    var i = e.id.replace('sendbutton','');
    var street = $('#tdstreet'+i).text();
    var building = $('#tdbuilding'+i).text();
    var message = $('#tdmessage'+i).val();
    var title = $('#tdtitle'+i).val();

    var content = new Object();
    content.street = street;
    content.building = building;
    content.message = message;
    content.title = title;

    var request = new XMLHttpRequest();
    request.open("POST", "/php/requestToForum.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==='1'){
        alert("Помилка! Перевірте дані!");
    }
    else {
        alert("Заявка була переміщена на форум");
        $('#deletebutton'+i).click();
    }
}