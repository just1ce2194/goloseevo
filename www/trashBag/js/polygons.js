function addCoordinate(){
    $('<input name="coords[]" type="text" placeholder="Coordinate"/><br>')
        .appendTo('#div');
}

function saveStreetToDB(){
    var name = $('#name').val();

    var req = new XMLHttpRequest();
    req.open("POST", "/php/saveStreetToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(name));

    if (req.responseText==="SUCCESS"){
        alert("Вулиця була добавлена в базу даних.")
        location.href="addstreet.html";
    }
    else{
        alert("Помилка БД!");
    }
}

function updateStreet(){
    var oldName = $('#street option:selected').text();
    var newName = $('#newName').val();
    var content = new Object();
    content.oldName = oldName;
    content.newName = newName;

    var req = new XMLHttpRequest();
    req.open("POST", "/php/updateStreetToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));

    if (req.responseText==="SUCCESS"){
        alert("Вулиця була оновлена в базі даних.")
        location.href="editstreets.html";
    }
    else{
        alert("Помилка БД!");
    }
}

function deleteStreet(){
    var name = $('#street option:selected').text();

    var req = new XMLHttpRequest();
    req.open("POST", "/php/deleteStreetFromDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(name));

    if (req.responseText==="SUCCESS"){
        alert("Вулиця \"" + name +"\" була видалена з бази даних.")
        location.href="editstreets.html";
    }
    else{
        alert("Помилка БД!");
    }
}

function saveBuildingToDB(){
    var street = $("#street option:selected").text();
    var building = $('#building').val();
    var coords = $('[name="coords[]"]');
    var newCoords = "";
    for (var i = 0; i < coords.length; i++) {
        newCoords+=coords[i].value+"; ";
    }
    var center = $('#center').val();
    var picture = $('#picture').val();
    var info = $('#info').val();
    var link = $('#link').val();

    var content = new Object();
    content.street = street;
    content.number = building;
    content.center = center;
    content.coordinates = newCoords;
    content.picture = picture;
    content.info = info;
    content.linktoforum = link;

    var req = new XMLHttpRequest();
    req.open("POST", "/php/saveBuildingToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));

    if (req.responseText==="SUCCESS"){
        alert("Будинок був добавлений в базу даних.")
        location.href="addbuilding.html";
    }
    else{
        alert("Помилка БД!");
    }
}

function getBuildings(){
    var req = new XMLHttpRequest();
    req.open("POST", "/php/getPolygons.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(null);

    var res = JSON.parse(req.responseText);
    if (res.length===0){
        alert("База будинків пуста.");
        return;
    }
    var divtable = $('#divtable');
    divtable.empty();
    var table = $('<table cellpadding="5"></table>');
    table.append($('<tr><th>№</th><th>Вулиця</th><th>Номер будинку</th><th>Координати центра</th><th>Координати</th><th>Картинка</th><th>Інформація</th><th>Посилання на форум</th></tr>'));
    for (var i=0;i<res.length;i++){
        var tr = $('<tr></tr>');
        tr.append($('<td>'+(i+1)+'</td>'));
        tr.append($('<td id="street'+i+'">'+res[i]['street']+'</td>'));
        tr.append($('<td id="building'+i+'">'+res[i]['number']+'</td>'));
        tr.append($('<td><input style="width: 130px" id="center'+i+'" value="'+res[i]['center']+'"></td>'));
        var coordinates = res[i]['coordinates'].split('; ');
        var text="";
        for (var j=0;j<coordinates.length-1;j++){
            text+=coordinates[j]+"\n";
        }
        tr.append($('<td><textarea rows="'+(coordinates.length)+'" cols="20" style="resize: none" id="coordinates'+i+'">'+text+'</textarea></td>'));
        tr.append($('<td><input id="picture'+i+'" value="'+res[i]['picture']+'"></td>'));
        tr.append($('<td><textarea rows="5" cols="20" style="resize:none" id="info'+i+'">'+res[i]['info']+'</textarea></td>'));
        tr.append($('<td><textarea rows="5" cols="20" style="resize:none" id="linktoforum'+i+'">'+res[i]['linktoforum']+'</textarea></td>'));
        tr.append($('<td><button id="edit'+i+'" onclick="editBuilding(this)">Edit</button><br><button id="delete'+i+'" onclick="deleteBuilding(this)">Delete</button> </td>'))
        table.append(tr);
    }
    divtable.append(table);
}

function editBuilding(e){
    var i = e.id.replace('edit','');
    var building = $('#building'+i).text();
    var center = $('#center'+i).val();
    var crds = $('#coordinates'+i).val();
    var coords = crds.split('\n');
    var coordinates = "";
    for (var j=0;j<coords.length-1;j++){
        coordinates+=coords[j]+"; ";
    }
    var picture = $('#picture'+i).val();
    var info = $('#info'+i).val();
    var linktoforum = $('#linktoforum'+i).val();

    var content = new Object();
    content.building = building;
    content.center = center;
    content.coordinates = coordinates;
    content.picture = picture;
    content.info = info;
    content.linktoforum = linktoforum;

    var request = new XMLHttpRequest();
    request.open("POST", "/php/updateBuildingToDB.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==="SUCCESS"){
        alert("Інформація про будинок \""+building+"\" була змінена.")
        getBuildings();
    }
    else{
        alert("DATABASE ERROR!");
    }
}

function deleteBuilding(e){
    var i = e.id.replace('delete','');
    var building = $('#building'+i).text();

    var request = new XMLHttpRequest();
    request.open("POST", "/php/deleteBuildingFromDB.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(building));

    if (request.responseText==="SUCCESS"){
        alert("Будинок \""+building+"\" був видалений з бази даних.")
        getBuildings();
    }
    else{
        alert("DATABASE ERROR!");
    }
}