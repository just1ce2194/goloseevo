var map;
var infoWindow;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        maxZoom:19,
        minZoom:15,
        zoom:14,
        center: {lat: 50.391683, lng: 30.496768},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var triangle = {
        A:[50.381299, 30.477409],
        B:[50.397716, 30.513244],
        C:[50.392928, 30.485134]
    };

    google.maps.event.addListener(map, 'dragend', function() {
        if (!pointInTriangle(map.getCenter().lat(),map.getCenter().lng(),triangle.A[0],triangle.A[1],triangle.B[0],triangle.B[1],triangle.C[0],triangle.C[1])){
            map.setCenter(new google.maps.LatLng(50.391683, 30.496768));
        }
    });

    var request = new XMLHttpRequest();
    request.open("GET", "/data/newMapObj.json", false);
    request.send(null);
    var my_JSON_object = JSON.parse(request.responseText);
    var streets = my_JSON_object["streets"];

    streets.forEach(function (street) {
            var buildings = street["buildings"];
            buildings.forEach(function (building) {
                    var coor = [];
                    var coordinates = building["coordinates"];
                    coordinates.forEach(function (coordinate) {
                        coor.push({lat: coordinate[0], lng: coordinate[1]});
                    });
                    var building1 = new google.maps.Polygon({
                        paths: coor,
                        strokeColor: '#a07dff',
                        strokeOpacity: 0.4,
                        strokeWeight: 3,
                        fillColor: '#c7b3ff',
                        fillOpacity: 0.2
                    });
                    building1.setMap(map);
                    building1.addListener('click', function () {
                        var contentString = '<FONT face="Helvetica Neue" size=3pt ><b>'+street["name"]+
                            ', будинок '+building["number"]+'</b><br><br><img src="'+building["picture"]+
                            '" alt="Smiley face" height="100" width="100" align="left" hspace="8">'+
                                building["info"]+'<br><a href="'+building["linktoforum"]+'">Перейти на форум</a></FONT>';
                        infoWindow.setContent(contentString);
                        infoWindow.setPosition({lat: building["center"][0], lng: building["center"][1]});
                        infoWindow.open(map);
                    });
                }
            )
        }
    );
    infoWindow = new google.maps.InfoWindow;
}

/*function is_in_triangle (px,py,ax,ay,bx,by,cx,cy){
    var v0 = [cx-ax,cy-ay];
    var v1 = [bx-ax,by-ay];
    var v2 = [px-ax,py-ay];

    var dot00 = (v0[0]*v0[0]) + (v0[1]*v0[1]);
    var dot01 = (v0[0]*v1[0]) + (v0[1]*v1[1]);
    var dot02 = (v0[0]*v2[0]) + (v0[1]*v2[1]);
    var dot11 = (v1[0]*v1[0]) + (v1[1]*v1[1]);
    var dot12 = (v1[0]*v2[0]) + (v1[1]*v2[1]);

    var invDenom = 1/ (dot00 * dot11 - dot01 * dot01);

    var u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    var v = (dot00 * dot12 - dot01 * dot02) * invDenom;

    return ((u >= 0) && (v >= 0) && (u + v < 1));
}*/

function sign (ax,ay,bx,by,cx,cy)
{
    return (ax - cx) * (by - cy) - (bx - cx) * (ay - cy);
}

/**
 * @return {boolean}
 */
function pointInTriangle (px,py,ax,ay,bx,by,cx,cy)
{
    var b1 = sign(px, py, ax, ay, bx, by) < 0.0;
    var b2 = sign(px, py, bx, by, cx, cy) < 0.0;
    var b3 = sign(px, py, cx, cy, ax, ay) < 0.0;

    return ((b1 === b2) && (b2 === b3));
}