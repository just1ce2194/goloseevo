function onChange(){
    $('#delete').remove();
    $('#imgplace').html('<img src="'+$('#picture option:selected').text()+'" width="100px"/>');
}

function getImages(){
    var request = new XMLHttpRequest();
    request.open("POST", "/php/getImages.php", false);
    request.send(null);
    if (request.responseText.length===0){
        alert('Таблиця \"images\" порожня!');
        return;
    }
    var res = JSON.parse(request.responseText);
    for (var i=0;i<res.length;i++){
        $('#picture').append($('<option value="'+res[i]['LINK']+'">'+res[i]['LINK']+'</option>'))
    }
}

function addNewsToDB(){
    var type = $('#type option:selected').val();
    var title = $('#title').val();
    var image =  $('#picture option:selected').val();
    var text = $('#textarea').val();

    var content = new Object();
    content.type = type;
    content.title = title;
    content.image = image;
    content.text = text;
    content.date = new Date();
    content.author = "";

    var req = new XMLHttpRequest();
    req.open("POST", "/php/saveNewsToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));
    if (req.responseText==="SUCCESS"){
        alert("Новина добавлена!");
        location.href="addnews.html";
    }
    else{
        alert("Помилка БД!");
    }
}

function getNews(type){
    var content = new Object();
    content.type = type;
    typeNews =type;
    var request = new XMLHttpRequest();
    request.open("POST", "/php/getNews.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText.length===0){
        alert('Таблиця \"'+type+'_news\" порожня');
        return;
    }

    var res = JSON.parse(request.responseText);
    var table = $('<table id="tableid"></table>');
    $('#divtable').empty();
    if (res.length!=0) {
        $('#divtable').append(table);
        $('#tableid').append($('<tr><th>№</th><th>Title</th><th>Image</th><th>Text</th><th>Date</th><th>Author</th></tr>'));
        for (var i = 0; i < res.length; i++) {
            $('#tableid').append($('<tr id="tr' + i + '"></tr>'));
            $('#tr' + i).append($('<td>' + (i + 1) + '</td>'));
            $('#tr' + i).append($('<td id="tdid' + i + '" hidden>' + res[i]['id'] + '</td>'));
            $('#tr' + i).append($('<td id="tdtitle' + i + '">' + res[i]['title'] + '</td>'));
            $('#tr' + i).append($('<td id="tdimage' + i + '"><img id="img'+i+'" src="' + res[i]['image'] + '" width="50px"/></td>'));
            $('#tr' + i).append($('<td id="tdtext' + i + '">' + res[i]['text'] + '</td>'));
            $('#tr' + i).append($('<td id="tddate' + i + '">' + res[i]['date'] + '</td>'));
            $('#tr' + i).append($('<td id="tdauthor' + i + '">' + res[i]['author'] + '</td>'));
            $('#tr' + i).append($('<td><button id="editbutton' + (i) + '" onclick="editNews(this)">' + 'Edit' + '</button></td>'));
            $('#tr' + i).append($('<td><button id="deletebutton' + (i) + '" onclick="newsToArchive(this)">' + 'Add to archive' + '</button></td>'));
        }
    }
    else{
        alert('Таблиця \"'+type+'_news\" порожня');
    }
}

function newsToArchive(e){
    var i = e.id.replace('deletebutton','');
    var id = $('#tdid'+i).text();

    var content = new Object();
    content.type = typeNews;
    content.id = id;

    var request = new XMLHttpRequest();
    request.open("POST", "/php/saveNewsToArchiveDB.php", false);
    request.setRequestHeader("Content-type", "application/json");
    request.send(JSON.stringify(content));

    if (request.responseText==="SUCCESS"){
        alert("Новина з id=\'"+id+"\' була перенесена в архів.")
        getNews(typeNews);
    }
    else{
        alert("Помилка БД!");
    }
}

function editNews(e){

    var i = e.id.replace('editbutton','');

    var id = $('<input hidden type="text" id="id" value="'+$('#tdid'+i).text()+'"/>')

    var title = $('<label>Заголовок:</label><br><input type="text" id="title" value="'+$('#tdtitle'+i).text()+'"/><br>');

    var request = new XMLHttpRequest();
    request.open("POST", "/php/getImages.php", false);
    request.send(null);
    if (request.responseText.length===0){
        alert('Таблиця \"images\" порожня!');
        return;
    }
    var picture = $('<label>Картинка:</label><br>');
    var select = $('<select id="picture" onchange="onChange()"></select><br>');
    var res = JSON.parse(request.responseText);
    for(var j=0;j<res.length;j++){
        if (res[j]['LINK']===$('#img'+i).attr("src")){
            select.append('<option selected value="'+res[j]['LINK']+'">'+res[j]['LINK']+'</option>');
        }
        else{
            select.append('<option value="'+res[j]['LINK']+'">'+res[j]['LINK']+'</option>');
        }
    }
    var imgplace = $('<div id="imgplace"></div>');

    var text = $('<label>Текст новини:</label><br><textarea rows="20" cols="100" id="textarea" required data-validation-required-message="Текст новини" maxlength="10000" style="resize:none">'+$('#tdtext'+i).text()+'</textarea><br>');

    var button = $('<button onclick="updateNewsToDB()">Редагувати новину</button>')

    $('#divtable').empty();
    $('#divtable').append(id);
    $('#divtable').append(title);
    $('#divtable').append(picture);
    $('#divtable').append(select);
    $('#divtable').append(imgplace);
    $('#divtable').append(text);
    $('#divtable').append(button);
}

function updateNewsToDB(){
    var id = $('#id').val();
    var title = $('#title').val();
    var image =  $('#picture option:selected').val();
    var text = $('#textarea').val();

    var content = new Object();
    content.type = typeNews;
    content.id = id;
    content.title = title;
    content.image = image;
    content.text = text;
    content.date = new Date();
    content.author = "";

    var req = new XMLHttpRequest();
    req.open("POST", "/php/updateNewsToDB.php", false);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify(content));

    if (req.responseText==="SUCCESS"){
        alert("Новина була оновлена.");
        getNews(typeNews);
    }
    else{
        alert("Помилка БД!");
    }
}