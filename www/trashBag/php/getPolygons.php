<?php
$str_json = file_get_contents('php://input');
include("db.php");
$query = sprintf("SELECT b.number, center, coordinates, picture, info, link_to_forum, s.name FROM streets s, buildings b where s.street_id=b.street");
$result = mysqli_query($db, $query) or die('Query failed: ' . mysql_error());;
$db->close();
$return_arr = array();
while ($row = mysqli_fetch_array($result)) {
    $row_array['street'] = $row[6];
    $row_array['number'] = $row[0];
    $row_array['center'] = $row[1];
    $row_array['coordinates'] = $row[2];
    $row_array['picture'] = $row[3];
    $row_array['info'] = $row[4];
    $row_array['linktoforum'] = $row[5];
    array_push($return_arr,$row_array);
}
echo json_encode($return_arr);
