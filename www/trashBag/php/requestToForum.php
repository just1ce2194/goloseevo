<?php

class TopicCreator
{
    function createTopic($input)
    {
        include("config.php");
        $forum_path = FORUM_ROOT; //FORUM FOLDER
        define('IPS_ENFORCE_ACCESS', TRUE);
        define('IPB_THIS_SCRIPT', 'public');

        require_once($forum_path . '\initdata.php');
        require_once($forum_path . '\admin\sources\base\ipsRegistry.php');
        require_once($forum_path . '\admin\sources\base\ipsController.php');

        $ipbRegistry = ipsRegistry::instance();
        $ipbRegistry->init();


        ipsRegistry::getAppClass('forums');

        $classToLoad = IPSLib::loadLibrary(IPSLib::getAppDir('forums') . '/sources/classes/post/classPost.php', 'classPost', 'forums');
        $classToLoad = IPSLib::loadLibrary(IPSLib::getAppDir('forums') . '/sources/classes/post/classPostForms.php', 'classPostForms', 'forums');
        $this->post = new $classToLoad($ipbRegistry);

        try {
            $this->post->setBypassPermissionCheck(true);
            $this->post->setIsAjax(false);
            $this->post->setPublished(true);
            include("getForumIdByStreetBuilding.php");
            $forum_dao = new ForumDao();
            $id = $forum_dao->getForumIdByStreetBuilding($input['street'],$input['building']);
            $this->post->setForumID($id);
            $this->post->setAuthor(IPSMember::load(1));
            $this->post->setPostContent($input['message']);
            $this->post->setTopicTitle($input['title']);
            $this->post->setSettings(array('enableSignature' => 1,
                'enableEmoticons' => 1,
                'post_htmlstatus' => 0));

            if ($this->post->addTopic() === false) {
                //print_r($this->post->getPostError());

                return false;
            }

            $topic = $this->post->getTopicData();
        } catch
        (Exception $e) {
            //print $e->getMessage();

            return false;
        }
        return $topic;
    }
}
$str_json = file_get_contents('php://input');
$jsonArray = json_decode($str_json, true);

$test_obj = new TopicCreator();
echo ($test_obj->createTopic($jsonArray)===false);