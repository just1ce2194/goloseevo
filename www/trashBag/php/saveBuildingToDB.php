<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
$query = sprintf("SELECT street_id FROM streets WHERE name='%s'",$jsonArray['street']);
$result = mysqli_query($db, $query);
while ($row = mysqli_fetch_array($result)) {
    $street_id = $row[0];
}
if ($stmt = $db->prepare('INSERT INTO buildings (number, center, coordinates, picture, info, link_to_forum, street) VALUES (?,?,?,?,?,?,?)')) {
    $stmt->bind_param('ssssssd',$jsonArray['number'], $jsonArray['center'], $jsonArray['coordinates'], $jsonArray['picture'], $jsonArray['info'],
        $jsonArray['linktoforum'], $street_id);
    $stmt->execute();
    $stmt->close();
}
echo "SUCCESS";
$db->close();