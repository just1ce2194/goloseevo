<?php
$str_json = file_get_contents('php://input');
include("db.php");
$query = sprintf("SELECT r.request_id, r.name, s.name, b.number, r.message, r.request_date FROM streets s, buildings b, unchecked_requests r
                  WHERE r.street=s.street_id and r.building=b.building_id");
$result = mysqli_query($db, $query) or die('Query failed: ' . mysql_error());
$db->close();
$res = array();
while ($row = mysqli_fetch_array($result)) {
    $arr['id'] = $row[0];
    $arr['name'] = $row[1];
    $arr['street'] = $row[2];
    $arr['building'] = $row[3];
    $arr['message'] = $row[4];
    $arr['date'] = $row[5];
    array_push($res,$arr);
}
echo json_encode($res);