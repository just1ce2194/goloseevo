<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if (strcasecmp($jsonArray['type'],"done_news")==0){
    if ($stmt = $db->prepare('INSERT INTO done_news (title, image, news_text, news_date, author) VALUES (?,
        (SELECT image_id FROM images i
        WHERE i.link=?)
        ,?,?,?)')) {
        $stmt->bind_param('sssss',$jsonArray['title'], $jsonArray['image'], $jsonArray['text'], $jsonArray['date'], $jsonArray['author']);
        $stmt->execute();
        $stmt->close();
    }
    echo "SUCCESS";
    $db->close();
}
else{
    if (strcasecmp($jsonArray['type'],"planned_news")==0){
        if ($stmt = $db->prepare('INSERT INTO planned_news (title, image, news_text, news_date, author) VALUES (?,
        (SELECT image_id FROM images i
        WHERE i.link=?)
        ,?,?,?)')) {
            $stmt->bind_param('sssss',$jsonArray['title'], $jsonArray['image'], $jsonArray['text'], $jsonArray['date'], $jsonArray['author']);
            $stmt->execute();
            $stmt->close();
        }
        echo "SUCCESS";
        $db->close();
    }
}