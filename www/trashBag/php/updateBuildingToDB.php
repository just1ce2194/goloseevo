<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('UPDATE buildings
                          SET center=?, coordinates=?, picture=?, info=?, link_to_forum=?
                          WHERE number=?')) {
    $stmt->bind_param('ssssss',$jsonArray['center'], $jsonArray['coordinates'], $jsonArray['picture'],
        $jsonArray['info'], $jsonArray['linktoforum'],$jsonArray['building']);
    $stmt->execute();
    $stmt->close();
}
echo "SUCCESS";
$db->close();
