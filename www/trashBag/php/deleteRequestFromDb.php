<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('DELETE FROM unchecked_requests WHERE request_id=?')) {
    $stmt->bind_param('d',$jsonArray['id']);
    $stmt->execute();
    $stmt->close();
}
echo "SUCCESS";
$db->close();