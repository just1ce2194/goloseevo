<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if ($stmt = $db->prepare('UPDATE streets SET name=? WHERE name=?')) {
        $stmt->bind_param('ss',$jsonArray['newName'], $jsonArray['oldName']);
        $stmt->execute();
        $stmt->close();
}
echo "SUCCESS";
$db->close();
