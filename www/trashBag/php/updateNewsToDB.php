<?php
$str_json = file_get_contents('php://input');
include("db.php");
$jsonArray = json_decode($str_json, true);
if (strcasecmp($jsonArray['type'],"done")==0){
    if ($stmt = $db->prepare('UPDATE done_news SET title=?, image=
        (SELECT image_id FROM images i
        WHERE i.link=?),
        news_text=?, news_date=?, author=?
        WHERE id=?')) {
        $stmt->bind_param('sssssd',$jsonArray['title'], $jsonArray['image'], $jsonArray['text'], $jsonArray['date'], $jsonArray['author'],$jsonArray['id']);
        $stmt->execute();
        $stmt->close();
    }
    echo "SUCCESS";
    $db->close();
}
else{
    if (strcasecmp($jsonArray['type'],"planned")==0){
        if ($stmt = $db->prepare('UPDATE planned_news SET title=?, image=
        (SELECT image_id FROM images i
        WHERE i.link=?),
        news_text=?, news_date=?, author=?
        WHERE id=?')) {
            $stmt->bind_param('sssssd',$jsonArray['title'], $jsonArray['image'], $jsonArray['text'], $jsonArray['date'], $jsonArray['author'],$jsonArray['id']);
            $stmt->execute();
            $stmt->close();
        }
        echo "SUCCESS";
        $db->close();
    }
}