<?php
class A
{
    function a()
    {
        $forum_path = 'Z:\home\goloseevo\www\forum'; //FORUM FOLDER
        define('IPS_ENFORCE_ACCESS', TRUE);
        define('IPB_THIS_SCRIPT', 'public');

        require_once($forum_path . '/initdata.php');
        require_once('Z:\home\goloseevo\www\forum\admin\sources\base\ipsRegistry.php');
        require_once('Z:\home\goloseevo\www\forum\admin\sources\base\ipsController.php');

        $ipbRegistry = ipsRegistry::instance();
        $ipbRegistry->init();


        ipsRegistry::getAppClass('forums');

        $classToLoad = IPSLib::loadLibrary(IPSLib::getAppDir('forums') . '/sources/classes/post/classPost.php', 'classPost', 'forums');
        $classToLoad = IPSLib::loadLibrary(IPSLib::getAppDir('forums') . '/sources/classes/post/classPostForms.php', 'classPostForms', 'forums');
        $this->post = new $classToLoad($ipbRegistry);

        try {
            $this->post->setBypassPermissionCheck(true);
            $this->post->setIsAjax(false);
            $this->post->setPublished(true);
            $this->post->setForumID(2);
            $this->post->setAuthor(IPSMember::load(1));
            $this->post->setPostContentPreFormatted("This is the already formatted content to store in the database");
            $this->post->setTopicTitle("This is the topic title");
            $this->post->setSettings(array('enableSignature' => 1,
                'enableEmoticons' => 1,
                'post_htmlstatus' => 0));

            if ($this->post->addTopic() === false) {
                print_r($this->post->getPostError());

                return false;
            }

            $topic = $this->post->getTopicData();
        } catch
        (Exception $e) {
            print $e->getMessage();

            return false;
        }
        return $topic;
    }
}
$test_obj = new A();
$test_obj->a();